#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

void TIM4_IRQHandler(void)
{
	//Verify if an interrupt happens
	if (TIM_GetITStatus(TIM4, TIM_IT_Update))
	{
		//Clear flag
		TIM_ClearITPendingBit(TIM4, TIM_IT_Update);

		//Toggle LEDs
		STM_EVAL_LEDToggle(LED3);
		STM_EVAL_LEDToggle(LED4);
		STM_EVAL_LEDToggle(LED5);
		STM_EVAL_LEDToggle(LED6);
	}
}

void Timer4_Init(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;

	/* Enable clock for TIM4 */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);

	/*
	TIM4 is connected to APB1 bus, which has on F407 device 42MHz clock
    But, timer has internal PLL, which double this frequency for timer, up to 84MHz
    Remember: Not each timer is connected to APB1, there are also timers connected
    on APB2, which works at 84MHz by default, and internal PLL increase
    this to up to 168MHz

    Set timer prescaller
    Timer count frequency is set with

    timer_tick_frequency = Timer_default_frequency / (prescaller_set + 1)

    In our case, we want a max frequency for timer, so we set prescaller to 0
    And our timer will have tick frequency

    timer_tick_frequency = 84000000 / (8399 + 1)
    timer_tick_frequency = 10000
    */

	TIM_TimeBaseInitStruct.TIM_Prescaler = 8399;

	/* Count up */
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;

	/*
    Set timer period when it have reset
    First you have to know max value for timer
    In our case it is 16bit = 65535
    To get your frequency for PWM, equation is simple

    PWM_frequency = timer_tick_frequency / (TIM_Period + 1)

    If you know your PWM frequency you want to have timer period set correct

    TIM_Period = (timer_tick_frequency / PWM_frequency) - 1

    In our case, for 1Hz PWM_frequency, set Period to

    TIM_Period = (10000 / 1) - 1
    TIM_Period = 9999

    If you get TIM_Period larger than max timer value (in our case 65535),
    you have to choose larger prescaler and slow down timer tick frequency
	*/
	TIM_TimeBaseInitStruct.TIM_Period = 9999;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0;

	/* Initialize TIM4 */
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStruct);

	//Enable TIM4 interruption
	TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);

	//NVIC interruptions (ARM cortex)
	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM4_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	/* Start count on TIM4 */
	TIM_Cmd(TIM4, ENABLE);
}

void Leds_Init(void)
{
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED4);
	STM_EVAL_LEDInit(LED5);
	STM_EVAL_LEDInit(LED6);

	STM_EVAL_LEDOn(LED3);
	STM_EVAL_LEDOn(LED4);
	STM_EVAL_LEDOn(LED5);
	STM_EVAL_LEDOn(LED6);
}

int main()
{
	Leds_Init();
	Timer4_Init();

	while(1);
}
