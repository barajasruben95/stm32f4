#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

volatile uint32_t ticks = 0;
uint8_t flag = 0;

void Systick_Init(uint16_t frequency)
{
	RCC_ClocksTypeDef RCC_Clocks;
	RCC_GetClocksFreq(&RCC_Clocks);
	(void) SysTick_Config(RCC_Clocks.HCLK_Frequency / frequency);
}

int main(void)
{
	Systick_Init(1000);

	STM_EVAL_LEDInit(LED4);
	STM_EVAL_LEDOff(LED4);

	while(1)
	{
		if(flag == 1)
		{
			flag = 0;
			STM_EVAL_LEDToggle(LED4);
		}
	}
}

void SysTick_Handler(void)
{
	ticks++;
	if(ticks > 1000)
	{
		ticks = 0;
		flag = 1;
	}
}
