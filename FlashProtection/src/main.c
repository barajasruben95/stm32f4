#include "stm32f4_discovery.h"
#include "stm32f4xx.h"

#define ADDR_FLASH_SECTOR_0     ((uint32_t)0x08000000) /* Base @ of Sector 0, 16 Kbytes */
#define ADDR_FLASH_SECTOR_1     ((uint32_t)0x08004000) /* Base @ of Sector 1, 16 Kbytes */
#define ADDR_FLASH_SECTOR_2     ((uint32_t)0x08008000) /* Base @ of Sector 2, 16 Kbytes */
#define ADDR_FLASH_SECTOR_3     ((uint32_t)0x0800C000) /* Base @ of Sector 3, 16 Kbytes */
#define ADDR_FLASH_SECTOR_4     ((uint32_t)0x08010000) /* Base @ of Sector 4, 64 Kbytes */
#define ADDR_FLASH_SECTOR_5     ((uint32_t)0x08020000) /* Base @ of Sector 5, 128 Kbytes */
#define ADDR_FLASH_SECTOR_6     ((uint32_t)0x08040000) /* Base @ of Sector 6, 128 Kbytes */
#define ADDR_FLASH_SECTOR_7     ((uint32_t)0x08060000) /* Base @ of Sector 7, 128 Kbytes */
#define ADDR_FLASH_SECTOR_8     ((uint32_t)0x08080000) /* Base @ of Sector 8, 128 Kbytes */
#define ADDR_FLASH_SECTOR_9     ((uint32_t)0x080A0000) /* Base @ of Sector 9, 128 Kbytes */
#define ADDR_FLASH_SECTOR_10    ((uint32_t)0x080C0000) /* Base @ of Sector 10, 128 Kbytes */
#define ADDR_FLASH_SECTOR_11    ((uint32_t)0x080E0000) /* Base @ of Sector 11, 128 Kbytes */

#define FLASH_USER_START_ADDR   ADDR_FLASH_SECTOR_1		/* Start @ of user Flash area */
#define FLASH_USER_END_ADDR     ADDR_FLASH_SECTOR_3   	/* End @ of user Flash area */

#define DATA_32                 ((uint32_t)0x12345678)

uint32_t FirstSector = 0;
uint32_t NbOfSectors = 0;
uint32_t Address = 0;
uint32_t SectorError = 0;
volatile uint32_t data32 = 0;
volatile uint32_t MemoryProgramStatus = 0;

/* Variable used for Erase procedure */
FLASH_EraseInitTypeDef EraseInitStruct;

void SystemClock_Config(void);
void Error_Handler(void);
uint32_t GetSector(uint32_t Address);

int main(void)
{
	HAL_Init();

	/* Initialize LED3, LED4, LED5 LED6 */
	BSP_LED_Init(LED3);
	BSP_LED_Init(LED4);
	BSP_LED_Init(LED5);
	BSP_LED_Init(LED6);

	/* Configure the system clock to 168 Mhz */
	SystemClock_Config();

	/*
	 * La siguiente seccion permite aplicar READ PROTECTION y WRITE PROTECTION
	 * Si el codigo de a continuacion se deja comentado, los sectores 1, 2 y 3 seran escritos con el dato 0x12345678.
	 * Si el codigo se descomenta, se intentara volver a escribir el dato 0x12345678 pero como los sectores 1, 2 y 3
	 * 		se protegeran con la instruccion FLASH->OPTCR &= 0xF<<28 | 0b111111110001<<16 | 0xAA<<8 | 0xFF, el programa
	 * 		sera enviado a la rutina Error_Handler().
	 * Si se comenta la linea FLASH->OPTCR &= 0xF<<28 | 0b111111110001<<16 | 0xAA<<8 | 0xFF y si se descomenta la linea
	 * 		FLASH->OPTCR &= 0xF<<28 | 0b111111111111<<16 | 0xAA<<8 | 0xFF, los sectores 1, 2 y 3 volveran a ser programados
	 * 		nuevamente porque se le quito el nivel de proteccion de escritura.
	 * Si la linea FLASH->OPTCR &= 0xF<<28 | 0b111111110001<<16 | 0xAA<<8 | 0xFF y/o FLASH->OPTCR &= 0xF<<28 | 0b111111111111<<16 | 0xAA<<8 | 0xFF;
	 * 		se comenta pero la instruccion FLASH->OPTCR |= 0x33<<8 se descomenta, se entrara al modo de proteccion nivel 1
	 * 		de lectura. En este modo si se intenta debuggear no sera posible. Si la tarjeta se intenta programar nuevamente
	 * 		aparecera un error de Read Protection lo cual indica que existe acceso limitado a leer los datos de la memoria.
	 * 		Solo a traves del ST-Link Utility sera posible resetear el nivel de read protection
	 * 		(abrir el programa, connect to target, ctrol-B, poner nivel de read protection a cero).
	*/
	//********************************************
//	/* Unlock OPTCR register */
//	HAL_FLASH_OB_Unlock();
//
//	/*  Check no Flash memory operation is ongoing */
//	while((FLASH->SR & 1<<16) != 0);
//
//	/* Write the desired value to OPTCR register (READ PROTECTION) */
//	//FLASH->OPTCR |= 0xAA<<8;		//Set level 0 read protection.
//	//FLASH->OPTCR |= 0x33<<8;		//Set level 1 read protection.
//	//FLASH->OPTCR |= 0xCC<<8;		//Set level 2 read protection.
//
//	/* Write the desired value to OPTCR register (WRITE PROTECTION) */
//	FLASH->OPTCR &= 0xF<<28 | 0b111111110001<<16 | 0xAA<<8 | 0xFF;	//Sector 1, 2 and 3 protected.
//	//FLASH->OPTCR &= 0xF<<28 | 0b111111111111<<16 | 0xAA<<8 | 0xFF;	//All sectors unprotected.
//
//	/* Set the option start bit OPTCR register */
//	FLASH->OPTCR |= 1<<1;
//
//	/*  Check no Flash memory operation is ongoing */
//	while((FLASH->SR & 1<<16) != 0);
//
//	/* Lock OPTCR register*/
//	HAL_FLASH_OB_Lock();
	//********************************************

	/* Unlock the Flash to enable the flash control register access */
	HAL_FLASH_Unlock();

	/* Get the 1st sector to erase */
	FirstSector = GetSector(FLASH_USER_START_ADDR);

	/* Get the number of sector to erase from 1st sector*/
	NbOfSectors = GetSector(FLASH_USER_END_ADDR) - FirstSector + 1;

	/* Fill EraseInit structure*/
	EraseInitStruct.TypeErase = TYPEERASE_SECTORS;
	EraseInitStruct.VoltageRange = VOLTAGE_RANGE_3;
	EraseInitStruct.Sector = FirstSector;
	EraseInitStruct.NbSectors = NbOfSectors;

	if (HAL_FLASHEx_Erase(&EraseInitStruct, &SectorError) != HAL_OK)
	{
		Error_Handler();
	}

	/* Program the user Flash area word by word (area defined by FLASH_USER_START_ADDR and FLASH_USER_END_ADDR) */
	Address = FLASH_USER_START_ADDR;

	while (Address < FLASH_USER_END_ADDR)
	{
		if (HAL_FLASH_Program(TYPEPROGRAM_WORD, Address, DATA_32) == HAL_OK)
		{
			Address = Address + 4;
		}
		else
		{
			Error_Handler();
		}
	}

	/* Lock the Flash to disable the flash control register access (recommended to protect the FLASH memory against possible unwanted operation) */
	HAL_FLASH_Lock();

	/* Check if the programmed data is OK
      MemoryProgramStatus = 0: data programmed correctly
      MemoryProgramStatus != 0: number of words not programmed correctly */
	Address = FLASH_USER_START_ADDR;
	MemoryProgramStatus = 0x0;

	while (Address < FLASH_USER_END_ADDR)
	{
		data32 = *(volatile uint32_t*)Address;

		if (data32 != DATA_32)
		{
			MemoryProgramStatus++;
		}

		Address = Address + 4;
	}

	/*Check if there is an issue to program data*/
	if (MemoryProgramStatus == 0)
	{
		/* No error detected. Switch on LED4*/
		BSP_LED_On(LED4);
	}
	else
	{
		/* Error detected. Switch on LED5*/
		Error_Handler();
	}

	/* Infinite loop */
	while (1);
}

// Gets the sector of a given address
uint32_t GetSector(uint32_t Address)
{
	uint32_t sector = 0;

	if((Address < ADDR_FLASH_SECTOR_1) && (Address >= ADDR_FLASH_SECTOR_0))
	{
		sector = FLASH_SECTOR_0;
	}
	else if((Address < ADDR_FLASH_SECTOR_2) && (Address >= ADDR_FLASH_SECTOR_1))
	{
		sector = FLASH_SECTOR_1;
	}
	else if((Address < ADDR_FLASH_SECTOR_3) && (Address >= ADDR_FLASH_SECTOR_2))
	{
		sector = FLASH_SECTOR_2;
	}
	else if((Address < ADDR_FLASH_SECTOR_4) && (Address >= ADDR_FLASH_SECTOR_3))
	{
		sector = FLASH_SECTOR_3;
	}
	else if((Address < ADDR_FLASH_SECTOR_5) && (Address >= ADDR_FLASH_SECTOR_4))
	{
		sector = FLASH_SECTOR_4;
	}
	else if((Address < ADDR_FLASH_SECTOR_6) && (Address >= ADDR_FLASH_SECTOR_5))
	{
		sector = FLASH_SECTOR_5;
	}
	else if((Address < ADDR_FLASH_SECTOR_7) && (Address >= ADDR_FLASH_SECTOR_6))
	{
		sector = FLASH_SECTOR_6;
	}
	else if((Address < ADDR_FLASH_SECTOR_8) && (Address >= ADDR_FLASH_SECTOR_7))
	{
		sector = FLASH_SECTOR_7;
	}
	else if((Address < ADDR_FLASH_SECTOR_9) && (Address >= ADDR_FLASH_SECTOR_8))
	{
		sector = FLASH_SECTOR_8;
	}
	else if((Address < ADDR_FLASH_SECTOR_10) && (Address >= ADDR_FLASH_SECTOR_9))
	{
		sector = FLASH_SECTOR_9;
	}
	else if((Address < ADDR_FLASH_SECTOR_11) && (Address >= ADDR_FLASH_SECTOR_10))
	{
		sector = FLASH_SECTOR_10;
	}
	else /* (Address < FLASH_END_ADDR) && (Address >= ADDR_FLASH_SECTOR_11) */
	{
		sector = FLASH_SECTOR_11;
	}

	return sector;
}

void Error_Handler(void)
{
	/* Turn LED5 (RED) on */
	BSP_LED_On(LED5);
	while(1);
}

/**
 * @brief  System Clock Configuration
 *            System Clock source            = PLL (HSE)
 *            SYSCLK(Hz)                     = 168000000
 *            HCLK(Hz)                       = 168000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 4
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 8000000
 *            PLL_M                          = 8
 *            PLL_N                          = 336
 *            PLL_P                          = 2
 *            PLL_Q                          = 7
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 5
 */
void SystemClock_Config(void)
{
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_OscInitTypeDef RCC_OscInitStruct;

	/* Enable Power Control clock */
	__PWR_CLK_ENABLE();

	/* The voltage scaling allows optimizing the power consumption when the device is
     clocked below the maximum system frequency, to update the voltage scaling value
     regarding system frequency refer to product datasheet.  */
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/* Enable HSE Oscillator and activate PLL with HSE as source */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	HAL_RCC_OscConfig(&RCC_OscInitStruct);

	/* Select PLL as system clock source and configure the HCLK, PCLK1 and PCLK2
     clocks dividers */
	RCC_ClkInitStruct.ClockType = (RCC_CLOCKTYPE_SYSCLK | RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2);
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;
	HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5);
}
