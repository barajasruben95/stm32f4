#include "stm32f4xx.h"

#define TIM3_FREQ	84000000
#define BAUDRATE_2400		35000
#define BAUDRATE_4800		17500
#define BAUDRATE_9600		8745
#define BAUDRATE_19200 		4375
#define BAUDRATE_115200		712

uint8_t _DataReady;

void SoftwareUART_Init(uint16_t baudrate);
void SoftwareUART_Send(uint8_t data);
