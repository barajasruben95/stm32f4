#include "stm32f4xx.h"
#include "SoftwareUART.h"

uint8_t variable = 0x55;

int main(void){

	SoftwareUART_Init(BAUDRATE_9600);

	for(;;)
	{
		if(1 == _DataReady)
		{
			_DataReady = 0;
			SoftwareUART_Send(variable);
		}
	}
}
