#include "SoftwareUART.h"

uint8_t _DataReady;

//Transmision.
uint16_t _Baudrate;
uint8_t _DataTx;
uint8_t _CantidadBitsTx;

//Recepcion.
uint16_t _SamplingBit;
uint8_t _DataRx;
uint8_t _CantidadBitsRx;

uint8_t _RxBuffer[64];
uint8_t _RxBufferIndex = 0;

void SoftwareUART_Init(uint16_t baudrate)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	TIM_OCInitTypeDef  TIM_OCInitStructure;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	EXTI_InitTypeDef EXTI_InitStruct;
	RCC_ClocksTypeDef RCC_Clocks;

	//Habilitar reloj.
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOB , ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	RCC_GetClocksFreq(&RCC_Clocks);

	//Guardar baudrate y sampling bit.
	_Baudrate = baudrate;
	_SamplingBit = (3*baudrate)/2;

	//Comenzar con linea en IDDLE.
	GPIOD->BSRRL = GPIO_Pin_7;

	//Inicializar pin de transmision (TX).
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_7;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOD, &GPIO_InitStructure);

	//Inicializar pin de recepcion (RX).
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	//Pin para detectar start bit.
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB, EXTI_PinSource3);

	//Configuracion de interrupcion externa por start bit.
	EXTI_InitStruct.EXTI_Line = EXTI_Line3;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_Init(&EXTI_InitStruct);

	//Configuracion del timer.
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_Period = 0xFFFE;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	//Configuracion output compare.
	TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_Active;

	//Configuracion del canal 01 para output compare (TX).
	TIM_OCInitStructure.TIM_Pulse = _Baudrate;
	TIM_OC1Init(TIM3, &TIM_OCInitStructure);

	//Configuracion del canal 02 para output compare (RX).
	TIM_OCInitStructure.TIM_Pulse = _Baudrate;
	TIM_OC2Init(TIM3, &TIM_OCInitStructure);

	//Configurar interrupcion del timer.
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 5;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 5;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	//Configurar interrupcion por start bit.
	NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	//Por el momento deshabilitar interrupcion del timer.
	TIM_ClearITPendingBit(TIM3, TIM_IT_CC1);
	TIM_ClearITPendingBit(TIM3, TIM_IT_CC2);
	TIM_ITConfig(TIM3, TIM_IT_CC1 | TIM_IT_CC2, DISABLE);

	//Habilitar el envio de datos.
	_DataReady = 1;

	//Activar timer y comenzar a correr.
	TIM_Cmd(TIM3, ENABLE);
}

void SoftwareUART_Send(uint8_t data)
{
	TIM_ClearITPendingBit(TIM3, TIM_IT_CC1);	//Asegurar flag del timer limpio.
	TIM3->CCR1 = TIM3->CNT + _Baudrate; 		//Cambiar el valor a comparar.

	_CantidadBitsTx = 10;						//Cantidad de bits a transmitir. 1 start, 8 datos, 1 stop.
	GPIOD->BSRRH = GPIO_Pin_7;					//Generar el start bit.
	_DataTx = data;
	TIM_ITConfig (TIM3, TIM_IT_CC1, ENABLE);	//Habilitar interrupcion del timer, comenzar transmision.
}

void TIM3_IRQHandler(void)
{
	//Para la transmision de datos.
	if (TIM_GetITStatus(TIM3, TIM_IT_CC1) != RESET)
	{
		//Apagar bandera.
		TIM_ClearITPendingBit(TIM3, TIM_IT_CC1);

		//Restar la cantidad de bits a transmitir.
		--_CantidadBitsTx;

		//Enviar bit a transmitir.
		if(_CantidadBitsTx < 10 && _CantidadBitsTx > 1)
		{
			if(_DataTx & 1)
				GPIOD->BSRRL = GPIO_Pin_7;
			else
				GPIOD->BSRRH = GPIO_Pin_7;
			TIM3->CCR1 = TIM3->CCR1 + _Baudrate;	//Cambiar el valor a comparar para poder transmitir otro bit.
			_DataTx>>=1;
		}

		if(_CantidadBitsTx == 1)
		{
			GPIOD->BSRRL = GPIO_Pin_7;				//Stop bit.
			TIM3->CCR1 = TIM3->CCR1 + _Baudrate;	//Cambiar el valor a comparar para el tiempo de duracion del stop bit.
		}

		if(_CantidadBitsTx == 0)
		{
			//Ya se mandaron todos los datos. Deshabilitar interrupcion del timer.
			TIM_ITConfig(TIM3, TIM_IT_CC1, DISABLE);
			_DataReady = 1;							//Indicarle al usuario que el envio de datos se ha completado.
		}
	}

	//Para la recepcion de datos.
	if (TIM_GetITStatus (TIM3, TIM_IT_CC2) != RESET)
	{
		//Apagar bandera.
		TIM_ClearITPendingBit(TIM3, TIM_IT_CC2);

		//Verificar dato de entrada.
		if(_CantidadBitsRx < 8)
		{
			_DataRx >>= 1;
			if((GPIOB->IDR & GPIO_Pin_3) != 0)
				_DataRx |= 0x80;
			++_CantidadBitsRx;
			TIM3->CCR2 = TIM3->CCR2 + _Baudrate;
		}
		else
		{
			/* Guardamos el dato recibido en el buffer de recepción */
			_RxBuffer[_RxBufferIndex++%64] = _DataRx;
			_CantidadBitsRx = 0;

			/* Deshabilitamos el muestreo de la entrada */
			_DataReady = 1;
			TIM_ITConfig(TIM3, TIM_IT_CC2, DISABLE);

			/* Habilitamos la interrupción para el start bit */
			EXTI_ClearITPendingBit(EXTI_Line3);
			NVIC_EnableIRQ(EXTI3_IRQn);
		}
	}
}

void EXTI3_IRQHandler(void)
{
	if(EXTI_GetITStatus(EXTI_Line3) != RESET)
	{
		//Limpiar bandera.
		EXTI_ClearITPendingBit(EXTI_Line3);

		//Deshabilitar interrupcion externa.
		NVIC_DisableIRQ(EXTI3_IRQn);
		_DataReady = 0;

		/* Configuramos para que el canal 02 del OC comienze a interrumpir */
		TIM_ClearITPendingBit(TIM3, TIM_IT_CC2);
		TIM3->CCR2 = TIM3->CNT + _SamplingBit;
		TIM_ITConfig(TIM3,TIM_IT_CC2, ENABLE);
	}
}
