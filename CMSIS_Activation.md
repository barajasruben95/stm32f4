# CMSIS activation

1. First, create a folder called **lib** in which the **libarm_cortexM4lf_math.a** file must be copied
https://bitbucket.org/barajasruben95/stm32f4/downloads/cmsis_01.jpg

1. Open **project properties**. Add a new preprocesor symbol over the following path: **C/C++ Build->Settings->MCU GCC Compiler->Preprocessor**. The preprocessor symbol is **ARM_MATH_CM4**
https://bitbucket.org/barajasruben95/stm32f4/downloads/cmsis_02.jpg
https://bitbucket.org/barajasruben95/stm32f4/downloads/cmsis_03.jpg
https://bitbucket.org/barajasruben95/stm32f4/downloads/cmsis_04.jpg

1. Once included the preprocessor symbol, go to the following path **C/C++ Build->Settings->MCU GCC Linker->Libraries** and add the file name and folder path respectively. Be careful, **DONT** write neither file extension nor the words lib
https://bitbucket.org/barajasruben95/stm32f4/downloads/cmsis_05.jpg
https://bitbucket.org/barajasruben95/stm32f4/downloads/cmsis_06.jpg
https://bitbucket.org/barajasruben95/stm32f4/downloads/cmsis_07.jpg

1. Click OK in order to save changes, include the library into the source code as follow and compile project
```C
#include "arm_math.h"

void main(void)
{
	for(;;);
}
```
