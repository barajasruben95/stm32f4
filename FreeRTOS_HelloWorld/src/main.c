/**
  ******************************************************************************
  * @file    main.c
  * @author  Ac6
  * @version V1.0
  * @date    01-December-2013
  * @brief   Default main function.
  ******************************************************************************
*/

#include "stm32f4xx.h"
#include "FreeRTOS.h"
#include "task.h"
#ifdef USE_SEMIHOSTING
#include <stdio.h>
#endif

/* Global variables */
TaskHandle_t xTaskHandle1 = NULL;
TaskHandle_t xTaskHandle2 = NULL;

/* Prototypes functions */
void vTask1_handler (void *params);
void vTask2_handler (void *params);
#ifdef USE_SEMIHOSTING
extern void initialise_monitor_handles(); /* Used for semi-hosting */
#endif

int main(void)
{
#ifdef USE_SEMIHOSTING
	/* Enable semi-hosting feature to print data into the eclipse console. Will work with debugger */
	initialise_monitor_handles();
	printf("FreeRTOS Hello World\n\r");
#endif

	/* Enable CYCCNT in DWT_CTRL for SEGGER. Timestamp will be shown in the applicacion */
	DWT->CTRL |= (1 << 0);

	/* Reset the RCC clock configuration: HSI ON, PLL OFF, system clock = 16MHz, CPU clock = 16MHz */
	RCC_DeInit();

	/* Update the SystemCoreClock variable */
	SystemCoreClockUpdate();

	/* Start recording with SEGGER */
	SEGGER_SYSVIEW_Conf();
	SEGGER_SYSVIEW_Start();

	/* Task creation */
	xTaskCreate(vTask1_handler, /* Pointer to the task entry function */
	             "Task1", /* A descriptive name for the task */
	             configMINIMAL_STACK_SIZE, /* The number of words (not bytes!) to allocate for use as the task�s stack */
	             NULL, /* Value that will passed into the created task as the task�s parameter */
	             2, /* The priority at which the created task will execute */
				 &xTaskHandle1 /* Used to pass a handle to the created task out of the xTaskCreate() function */
	             );
	xTaskCreate(vTask2_handler, /* Pointer to the task entry function */
	 	         "Task2", /* A descriptive name for the task */
	 	         configMINIMAL_STACK_SIZE, /* The number of words (not bytes!) to allocate for use as the task�s stack */
	 	         NULL, /* Value that will passed into the created task as the task�s parameter */
	 	         2, /* The priority at which the created task will execute */
	 			 &xTaskHandle2 /* Used to pass a handle to the created task out of the xTaskCreate() function */
	 	         );

	/* Start running the scheduler */
	vTaskStartScheduler();

	for(;;);
}

/**
 * @brief Task 1
 * @param[in] params Input parameter to the task. It can be whatever due to void
 * @return void
 */
void vTask1_handler (void *params)
{
	while(1)
	{
#ifdef USE_SEMIHOSTING
		printf("Task 1\n\r");
#endif
	}
}

/**
 * @brief Task 2
 * @param[in] params Input parameter to the task. It can be whatever due to void
 * @return void
 */
void vTask2_handler (void *params)
{
	while(1)
	{
#ifdef USE_SEMIHOSTING
		printf("Task 2\n\r");
#endif
	}
}
