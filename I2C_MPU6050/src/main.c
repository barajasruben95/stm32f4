#include <math.h>
#include "stm32f4xx.h"
#include <stm32f4xx_i2c.h>
#include <Delay.h>
#include <UART.h>
#include <I2C.h>
#include <I2C_MPU6050.h>

#define MPU6050_ADDRESS 0xD0

int main(void)
{
	MPU6050 myMPU6050;
	//double magnitudVectorAceleracion;

	Delay__Init();
	I2C__Init();
	UART__Init();

	I2C__MPU6050_Init(MPU6050_ADDRESS);
	UART__WriteString("OK\n\r");

	//Configuracion 8G
	uint8_t valorConfiguracionAcelerometro = I2C__MPU6050_ReadAccelerometerConfig(MPU6050_ADDRESS);
	I2C__MPU6050_WriteAccelerometerConfig(MPU6050_ADDRESS, (valorConfiguracionAcelerometro & 0b11100111) | 0b00010000);

	while(1)
	{
		//Esperar hasta que la lectura este completada
		if(I2C__MPU6050_Read(MPU6050_ADDRESS, &myMPU6050))
		{
			UART__WriteString("X: ");
			UART__WriteFloatingNumber(myMPU6050.AcelerometerAngleX, 2);

			UART__WriteString("    Y: ");
			UART__WriteFloatingNumber(myMPU6050.AcelerometerAngleY, 2);
			UART__WriteString("\n\r");

//			//Aceleracion de la gravedad
//			magnitudVectorAceleracion = sqrt(pow((double)myMPU6050.AcelerometerX_m_s,(double)2) + pow((double)myMPU6050.AcelerometerY_m_s,(double)2) + pow((double)myMPU6050.AcelerometerZ_m_s,(double)2));
//			if(magnitudVectorAceleracion >= (double)ACELERACION_GRAVEDAD*3)
//			{
//				UART__WriteString("<----- ACELERACION SUPERADA ----->\n\r");
//				UART__WriteFloatingNumber((float)magnitudVectorAceleracion, 4);
//				UART__Write('\n'); UART__Write('\r');
//			}
//			else
//			{
//				UART__WriteString("Magnitud aceleracion: ");
//				UART__WriteFloatingNumber((float)magnitudVectorAceleracion, 4);
//				UART__Write('\n'); UART__Write('\r');
//			}

//			//Temperatura
//			UART__WriteFloatingNumber(myMPU6050.Temperature, 2);
//			UART__Write('\n'); UART__Write('\r');

			Delay__MS(100); //Retardo entre lectura y lectura
		}
	}
}
