/**
 * @file    main.c
 * @date    24-February-2024
 * @brief   STM32F4-Discovery. Bootloader BL.
 * @author  Ruben Barajas
 * @email   barajas.ruben95@gmail.com
 * @website Bitbucket - barajasruben95
 * @link    https://bitbucket.org/barajasruben95/
 * @version v1.0
 * @ide     System Workbench for STM32
 */

/*
 * DISCLAIMER:
 * The following code is sourced from the repository available at
 * https://github.com/niekiran/BootloaderProjectSTM32
 * This code is provided as-is without any warranty of any kind,
 * either express or implied, including but not limited to the implied warranties
 * of merchantability, fitness for a particular purpose, or non-infringement.
 * The owner(s) of the repository, Kiran Nayak, shall not be held liable for any
 * damages arising from the use of this code.
*/

/**
 * INSTRUCTIONS:
 * 1.-  COMPILE THE BOOTLOADER_BL PROJECT
 *      Compile this project (BOOTLOADER_BL) and generate the hex/bin file.
 *
 * 2.-  COMPILE THE USER APPLICATION PROJECT (EX: BOOTLOADER_APP PROJECT)
 *      Compile the application project and generate the hex/bin file.
 *      IMPORTANT: as the bootloader is using SECTOR 0, SECTOR 1 and SECTOR 2, ensure
 *                 the user application will be flashed in other available sectors.
 *                 Modify the linker file and move the reset vector table accordingly.
 *
 *                 I)
 *                 Change the vector table to sector 3
 *                 File: system_stm32f4xx.c
 *                 VECT_TAB_OFFSET  0xC000
 *
 *                 II)
 *                 Change the FLASH region to flash the program in sector 3
 *                 File: LinkerScript.ld
 *                 FLASH (rx)      : ORIGIN = 0x0800C000, LENGTH = 1024K
 *
 * 3.-  FLASH THE BOOTLOADER USING AN EXTERNAL UTILITY
 *      For some reason, flashing the STM32F407 Discovery with OpenSTM32, all sectors
 *      are flashed too (filled with zeros). Use an external flasher like STM32 ST-LINK utility.
 *
 * 4.-  CONNECT THE BOOTLOADER HOST WITH THE STM32F407 DISCOVERY
 *      External UART to TTL converter is needed to communicate the bootloader host with the
 *      bootloader. Use pins PA2(TX) and PA3(RX) to connect the board with the bootloader host.
 *
 * 5.-  RUN THE HOST BOOTLOADER
 *      The bootloader host is a python script located in the "Host" folder. It'executed using
 *      "python STM32_Programmer_V2.py". Set the serial port com from the previous step.
 *      Tested OK with python3.10.5.
 *
 * 6.-  ENTER TO BOOTLOADER MODE IN STM32F407 DISCOVERY
 *      To enter the STM32F407 board in bootloader mode, ensure to keep the USER push button pressed
 *      while the reset is pushed-released through the RESET button.
 *
 * 7.-  (OPTIONAL) CONNECT A DEBUGGER
 *      External UART to TTL converter is needed to see debug messages.
 *      Use pins PC10(TX) and PC11(RX) to connect the board with the PC to see UART messages.
 *
 * 8a.- FLASH THE USER APPLICATION USING AN EXTERNAL UTILITY
 *      If desired, flash the user application project too using an external flasher
 *      like STM32 ST-LINK utility.
 *
 * 8b.- FLASH THE USER APPLICATION USING THE BOOTLOADER
 *      Flash the user application using this bootloader, place the bin file generated from the
 *      user application in the "Host" folder and rename it to "user_app.bin".
 *      Use the command BL_MEM_WRITE to send the user application automatically.
 *
 * 9.   Extra documentation is found in the "doc" folder.
 */

#include "stm32f4xx_hal.h"
#include "stm32f4_discovery.h"
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

/** Enable debugging messages */
#define BL_DEBUG_MSG_EN
/** Address sector where the program should be flashed.
 * Modify the linker file and vector table start address accordingly too */
#define FLASH_SECTOR_3_BASE_ADDRESS 0x0800C000U

/* BOOTLOADER COMMANDS */

/** Bootloader version 1.0 */
#define BL_VERSION                              0x10

/** This command is used to read the bootloader version from the MCU */
#define BL_GET_VER								0x51

/** This command is used to know what are the commands supported by the bootloader */
#define BL_GET_HELP								0x52

/** This command is used to read the MCU chip identification number */
#define BL_GET_CID								0x53

/** This command is used to read the FLASH Read Protection level */
#define BL_GET_RDP_STATUS						0x54

/** This command is used to jump bootloader to specified address */
#define BL_GO_TO_ADDR							0x55

/** This command is used to mass erase or sector erase of the user flash */
#define BL_FLASH_ERASE    						0x56

/** This command is used to write data in to different memories of the MCU */
#define BL_MEM_WRITE							0x57

/** This command is used to enable or disable read/write protect on different sectors of the user flash */
#define BL_EN_RW_PROTECT  						0x58

/** This command is used to read data from different memories of the microcontroller */
#define BL_MEM_READ								0x59

/** This command is used to read all the sector protection status */
#define BL_READ_SECTOR_P_STATUS					0x5A

/** This command is used to read the OTP contents */
#define BL_OTP_READ								0x5B

/** This command is used disable all sector read/write protection */
#define BL_DIS_R_W_PROTECT						0x5C

/* ACK and NACK bytes*/
#define BL_ACK   0XA5 /** COMMAND RECOGNIZED AND ACCEPTED */
#define BL_NACK  0X7F /** COMMAND NOT RECOGNIZED */

/* CRC */
#define VERIFY_CRC_FAIL    1 /** CRC VERIFICATION SUCCESS */
#define VERIFY_CRC_SUCCESS 0 /** CRC VERIFICATION FAILED */

/* VALID ADDRESS */
#define ADDR_VALID      0x00 /** RETURN CODE FOR VALID ADDRESS SELECTED */
#define ADDR_INVALID    0x01 /** RETURN CODE FOR INVALID ADDRESS SELECTED */
#define INVALID_SECTOR  0x04 /** RETURN CODE FOR INVALID SECTOR SELECTED */

/* Some Start and End addresses of different memories of STM32F407 MCU */
#define SRAM1_SIZE            112*1024
#define SRAM1_END             (SRAM1_BASE + SRAM1_SIZE)
#define SRAM2_SIZE            16*1024
#define SRAM2_END             (SRAM2_BASE + SRAM2_SIZE)
#define FLASH_SIZE            1024*1024
#define BKPSRAM_SIZE          4*1024
#define BKPSRAM_END           (BKPSRAM_BASE + BKPSRAM_SIZE)

/* UART */
#define D_UART &huart3
#define C_UART &huart2

/* STATIC FUNCTIONS */
static void SystemClock_Config(void);
static void Error_Handler(void);
static void MX_CRC_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_USART3_UART_Init(void);

/* HELPER FUNCTIONS */
void printmsg(char *format, ...);
void bootloader_uart_read_data(void);
void bootloader_jump_to_user_application(void);

/* BOOTLOADER HELPER FUNCTION PROTOTYPES */

/* This function sends ACK if CRC matches along with "len to follow" */
void bootloader_send_ack(uint8_t command_code, uint8_t follow_len);
/* This function sends NACK */
void bootloader_send_nack(void);
/* This function writes data in to C_UART */
void bootloader_uart_write_data(uint8_t *pBuffer, uint32_t len);
/* Read the chip identifier or device identifier */
uint16_t get_mcu_chip_id(void);
/* This function reads the RDP ( Read protection option byte) value */
uint8_t get_flash_rdp_level(void);
/* Verify the address sent by the host is valid */
uint8_t verify_address(uint32_t go_address);
/* Perform flash erase - sector or full */
uint8_t execute_flash_erase(uint8_t sector_number, uint8_t number_of_sector);
/* Writes the contents of pBuffer to "mem_address" byte by byte */
uint8_t execute_mem_write(uint8_t *pBuffer, uint32_t mem_address, uint32_t len);
/* Modifying user option bytes for write protection sectors */
uint8_t configure_flash_sector_rw_protection(uint16_t sector_details, uint8_t protection_mode, uint8_t disable);
/* Read the user option bytes related to write protection sectors */
uint16_t read_OB_rw_protection_status(void);

/* BOOTLOADER FUNCTION PROTOTYPES */
void bootloader_handle_getver_cmd(uint8_t *bl_rx_buffer);
void bootloader_handle_gethelp_cmd(uint8_t *pBuffer);
void bootloader_handle_getcid_cmd(uint8_t *pBuffer);
void bootloader_handle_getrdp_cmd(uint8_t *pBuffer);
void bootloader_handle_go_cmd(uint8_t *pBuffer);
void bootloader_handle_flash_erase_cmd(uint8_t *pBuffer);
void bootloader_handle_mem_write_cmd(uint8_t *pBuffer);
void bootloader_handle_en_rw_protect(uint8_t *pBuffer);
void bootloader_handle_mem_read (uint8_t *pBuffer);
void bootloader_handle_read_sector_protection_status(uint8_t *pBuffer);
void bootloader_handle_read_otp(uint8_t *pBuffer);
void bootloader_handle_dis_rw_protect(uint8_t *pBuffer);

CRC_HandleTypeDef hcrc;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
uint8_t bl_rx_buffer[200];          /* Buffer to receive data from the host */
uint8_t supported_commands[] = {    /* Supported commands by the bootloader */
        BL_GET_VER,
        BL_GET_HELP,
        BL_GET_CID,
        BL_GET_RDP_STATUS,
        BL_GO_TO_ADDR,
        BL_FLASH_ERASE,
        BL_MEM_WRITE,
        BL_READ_SECTOR_P_STATUS
} ;

/**
 * @brief  Main program
 * @param  None
 * @retval None
 */
/**
@startuml
title BOOTLOADER VS USER APPLICATION HANDLER
:Reset MCU;
:**INITIALIZATION**
HAL
CLOCK
GPIO
UART
CRC
;
if (User button pressed?) then (yes)
  :**FUNCTION**\nbootloader_uart_read_data());
  stop
else (no)
  :**FUNCTION**\nbootloader_jump_to_user_app();
  stop
endif
@enduml
 */
int main(void)
{
    /* STM32F4xx HAL library initialization:
       - Configure the Flash prefetch, instruction and Data caches
       - Configure the Systick to generate an interrupt each 1 msec
       - Set NVIC Group Priority to 4
       - Global MSP (MCU Support Package) initialization
     */
    HAL_Init();

    /* Configure the system clock to 168 MHz */
    SystemClock_Config();

    /* Initialize all configured peripherals */
    BSP_PB_Init(BUTTON_KEY, BUTTON_MODE_GPIO);
    BSP_LED_Init(LED4);
    MX_CRC_Init();
    MX_USART2_UART_Init();
    MX_USART3_UART_Init();

    /* Check the push buttom status, pressed or released */
    if(BSP_PB_GetState(BUTTON_KEY) == GPIO_PIN_SET)
    {
        /* BOOTLOADER MODE */
        printmsg("BL_DEBUG_MSG: Button is pressed ... going to BL mode\n\r");
        bootloader_uart_read_data();
    }
    else
    {
        /* APPLICATION MODE */
        printmsg("BL_DEBUG_MSG: Button is not pressed ... going to user application\n\r");
        bootloader_jump_to_user_application();
    }

    /* Should never be executed, bootloader or user app now take the control */
    while(1);
}

/**
 * @brief  System Clock Configuration
 *         The system Clock is configured as follow :
 *            System Clock source            = PLL (HSI)
 *            SYSCLK(Hz)                     = 168000000
 *            HCLK(Hz)                       = 168000000
 *            AHB Prescaler                  = 1
 *            APB1 Prescaler                 = 4
 *            APB2 Prescaler                 = 2
 *            HSE Frequency(Hz)              = 8000000
 *            HSI Frequency(Hz)              = 16000000
 *            PLL_M                          = 8
 *            PLL_N                          = 168
 *            PLL_P                          = 2
 *            PLL_Q                          = 7
 *            VDD(V)                         = 3.3
 *            Main regulator output voltage  = Scale1 mode
 *            Flash Latency(WS)              = 5
 * @param  None
 * @retval None
 */
static void SystemClock_Config(void)
{
    RCC_OscInitTypeDef RCC_OscInitStruct = {0};
    RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

    /** Configure the main internal regulator output voltage */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

    /** Initializes the RCC Oscillators according to the specified parameters
     * in the RCC_OscInitTypeDef structure. */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
    RCC_OscInitStruct.PLL.PLLM = 8;
    RCC_OscInitStruct.PLL.PLLN = 168;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 7;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
        Error_Handler();
    }

    /** Initializes the CPU, AHB and APB buses clocks */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
    {
        Error_Handler();
    }

    /* STM32F405x/407x/415x/417x Revision Z devices: prefetch is supported  */
    if (HAL_GetREVID() == 0x1001)
    {
        /* Enable the Flash prefetch */
        __HAL_FLASH_PREFETCH_BUFFER_ENABLE();
    }
}

/**
 * @brief  Initialize the MSP.
 * @retval None
 */
void HAL_MspInit(void)
{
    __HAL_RCC_SYSCFG_CLK_ENABLE();
    __HAL_RCC_PWR_CLK_ENABLE();

    HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_0);
}

/**
 * @brief USART2 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART2_UART_Init(void)
{
    huart2.Instance = USART2;
    huart2.Init.BaudRate = 115200;
    huart2.Init.WordLength = UART_WORDLENGTH_8B;
    huart2.Init.StopBits = UART_STOPBITS_1;
    huart2.Init.Parity = UART_PARITY_NONE;
    huart2.Init.Mode = UART_MODE_TX_RX;
    huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart2.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart2) != HAL_OK)
    {
        Error_Handler();
    }
}

/**
 * @brief USART3 Initialization Function
 * @param None
 * @retval None
 */
static void MX_USART3_UART_Init(void)
{
    huart3.Instance = USART3;
    huart3.Init.BaudRate = 115200;
    huart3.Init.WordLength = UART_WORDLENGTH_8B;
    huart3.Init.StopBits = UART_STOPBITS_1;
    huart3.Init.Parity = UART_PARITY_NONE;
    huart3.Init.Mode = UART_MODE_TX_RX;
    huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart3.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart3) != HAL_OK)
    {
        Error_Handler();
    }
}

/**
 * @brief UART MSP Initialization
 * This function configures the hardware resources used in this example
 * @param huart: UART handle pointer
 * @retval None
 */
void HAL_UART_MspInit(UART_HandleTypeDef* huart)
{
    GPIO_InitTypeDef GPIO_InitStruct = {0};
    if(huart->Instance==USART2)
    {
        /* Peripheral clock enable */
        __HAL_RCC_USART2_CLK_ENABLE();

        __HAL_RCC_GPIOA_CLK_ENABLE();
        /**USART2 GPIO Configuration
        PA2     ------> USART2_TX
        PA3     ------> USART2_RX
         */
        GPIO_InitStruct.Pin = GPIO_PIN_2|GPIO_PIN_3;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART2;
        HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);
    }
    else if(huart->Instance==USART3)
    {
        /* Peripheral clock enable */
        __HAL_RCC_USART3_CLK_ENABLE();

        __HAL_RCC_GPIOC_CLK_ENABLE();
        /**USART3 GPIO Configuration
        PC10     ------> USART3_TX
        PC11     ------> USART3_RX
         */
        GPIO_InitStruct.Pin = GPIO_PIN_10|GPIO_PIN_11;
        GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
        GPIO_InitStruct.Pull = GPIO_NOPULL;
        GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
        GPIO_InitStruct.Alternate = GPIO_AF7_USART3;
        HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
    }
}

/**
 * @brief CRC Initialization Function
 * @param None
 * @retval None
 */
static void MX_CRC_Init(void)
{
    hcrc.Instance = CRC;
    if (HAL_CRC_Init(&hcrc) != HAL_OK)
    {
        Error_Handler();
    }
}

/**
 * @brief CRC MSP Initialization
 * This function configures the hardware resources used in this example
 * @param hcrc: CRC handle pointer
 * @retval None
 */
void HAL_CRC_MspInit(CRC_HandleTypeDef* hcrc)
{
    if(hcrc->Instance==CRC)
    {
        /* Peripheral clock enable */
        __HAL_RCC_CRC_CLK_ENABLE();
    }
}

/* Print formatted string to console over D_UART */
void printmsg(char *format, ...)
{
#ifdef BL_DEBUG_MSG_EN
    char str[80];

    /* Extract the argument list using VA apis */
    va_list args;
    va_start(args, format);
    vsprintf(str, format, args);
    HAL_UART_Transmit(D_UART, (uint8_t *)str, strlen(str), HAL_MAX_DELAY);
    va_end(args);
#endif
}

/** This function reads data from C_UART and decode and execute the desired bootloader function */
/**
@startuml
title BOOTLOADER COMMANDS HANDLER
while (**FUNCTION**\nbootloader_uart_read_data())
    repeat
      :Poll UART for data;
    repeat while (Length byte received?) is (no)
    ->yes;
    :Read "length" number \nof bytes;
    :Decode command;
    :Handle command\n...\n...\n...;
  endwhile
  -[hidden]->
  detach
@enduml
 */
void bootloader_uart_read_data(void)
{
    uint8_t rcv_len = 0;

    while(1)
    {
        memset(bl_rx_buffer, 0, 200);

        /* READ AND DECODE COMMANDS FROM HOST */

        /* 1. Read only one byte from host, which is the length to follow field */
        HAL_UART_Receive(C_UART, &bl_rx_buffer[0], 1, HAL_MAX_DELAY);
        rcv_len = bl_rx_buffer[0];
        /*  2. Read payload and CRC */
        HAL_UART_Receive(C_UART, &bl_rx_buffer[1], rcv_len, HAL_MAX_DELAY);

        /* Decode the command received */
        switch(bl_rx_buffer[1])
        {
            case BL_GET_VER:
                bootloader_handle_getver_cmd(bl_rx_buffer);
                break;
            case BL_GET_HELP:
                bootloader_handle_gethelp_cmd(bl_rx_buffer);
                break;
            case BL_GET_CID:
                bootloader_handle_getcid_cmd(bl_rx_buffer);
                break;
            case BL_GET_RDP_STATUS:
                bootloader_handle_getrdp_cmd(bl_rx_buffer);
                break;
            case BL_GO_TO_ADDR:
                bootloader_handle_go_cmd(bl_rx_buffer);
                break;
            case BL_FLASH_ERASE:
                bootloader_handle_flash_erase_cmd(bl_rx_buffer);
                break;
            case BL_MEM_WRITE:
                bootloader_handle_mem_write_cmd(bl_rx_buffer);
                break;
            case BL_EN_RW_PROTECT:
                bootloader_handle_en_rw_protect(bl_rx_buffer);
                break;
            case BL_MEM_READ:
                bootloader_handle_mem_read(bl_rx_buffer);
                break;
            case BL_READ_SECTOR_P_STATUS:
                bootloader_handle_read_sector_protection_status(bl_rx_buffer);
                break;
            case BL_OTP_READ:
                bootloader_handle_read_otp(bl_rx_buffer);
                break;
            case BL_DIS_R_W_PROTECT:
                bootloader_handle_dis_rw_protect(bl_rx_buffer);
                break;
            default:
                printmsg("BL_DEBUG_MSG:Invalid command code received from host\n\r");
                break;
        }
    }
}

/* JUMP TO USER APPLICATION
 * Application goes in SECTOR 3
 */
void bootloader_jump_to_user_application(void)
{
    /* Get address of the reset handler user aplication */
    void (*app_reset_handler) (void);
    printmsg("BL_DEBUG_MSG: bootloader jump to user application\n\r");

    /* 1. configure the MSP by reading the value from the base address of the sector 2
     * 0x0800C000 is the user application address + offset 0x0 = MSP value */
    uint32_t msp_value = *(volatile uint32_t *)FLASH_SECTOR_3_BASE_ADDRESS;
    printmsg("BL_DEBUG_MSG: MSP value: %#x\n\r", msp_value);

    /* Set MSP value using CMSIS instruction = SCB->VTOR = FLASH_SECTOR_3_BASE_ADDRESS */
    __set_MSP(msp_value);

    /* 2. fetch the reset handler address of the user application
     * 0x0800C000 is the user application address + offset 0x4 = Reset handler value */
    uint32_t resethandler_address = *(volatile uint32_t *) (FLASH_SECTOR_3_BASE_ADDRESS + 4);
    resethandler_address |= 1; /* Ensure T bit is set. ThumbII instruction */
    app_reset_handler = (void*) resethandler_address;
    printmsg("BL_DEBUG_MSG: app reset handler address: %#x\n\r", app_reset_handler);

    /* 3. jump to reset handler of the user application */
    app_reset_handler();
}

/** This function sends ACK if CRC matches along with "len to follow" */
/**
@startuml
title HANDLE COMMAND
start
if (Good CRC?) then (no)
:send NACK;
else (yes)
:send ACK;
:obtain reply;
:send reply;
endif
:Finish Handle Command;
:**FUNCTION**\nbootloader_uart_read_data();
@enduml
 */
void bootloader_send_ack(uint8_t command_code, uint8_t follow_len)
{
    /* Here we send 2 byte ... first byte is ack and the second byte is len value */
    uint8_t ack_buf[2];
    ack_buf[0] = BL_ACK;
    ack_buf[1] = follow_len;
    HAL_UART_Transmit(C_UART, ack_buf, 2, HAL_MAX_DELAY);
}

/** This function sends NACK */
/**
@startuml
title HANDLE COMMAND
start
if (Good CRC?) then (no)
:send NACK;
else (yes)
:send ACK;
:obtain reply;
:send reply;
endif
:Finish Handle Command;
:**FUNCTION**\nbootloader_uart_read_data();
@enduml
 */
void bootloader_send_nack(void)
{
    uint8_t nack = BL_NACK;
    HAL_UART_Transmit(C_UART, &nack, 1, HAL_MAX_DELAY);
}

/* This function writes data in to C_UART */
void bootloader_uart_write_data(uint8_t *pBuffer, uint32_t len)
{
    /* You can replace the below ST's USART driver API call with your MCUs driver API call */
    HAL_UART_Transmit(C_UART, pBuffer, len, HAL_MAX_DELAY);
}

/* This verifies the CRC of the given buffer in pData */
uint8_t bootloader_verify_crc(uint8_t *pData, uint32_t len, uint32_t crc_host)
{
    uint32_t uwCRCValue = 0xff;
    uint8_t retVal = VERIFY_CRC_FAIL;

    for(uint32_t i = 0; i < len; i++)
    {
        uint32_t i_data = pData[i];
        uwCRCValue = HAL_CRC_Accumulate(&hcrc, &i_data, 1);
    }

    /* Reset CRC Calculation Unit */
    __HAL_CRC_DR_RESET(&hcrc);

    if(uwCRCValue == crc_host)
    {
        retVal = VERIFY_CRC_SUCCESS;
    }

    return (retVal);
}

/* Read the chip identifier or device identifier */
uint16_t get_mcu_chip_id(void)
{
    /*
    The STM32F4xx MCUs integrate an MCU ID code. This ID identifies the ST MCU partnumber
    and the die revision. It is part of the DBG_MCU component and is mapped on the
    external PPB bus (see Section 38.16). This code is accessible using the JTAG debug port
    (four to five pins) or the SW debug port (two pins) or by the user software. It is even
    accessible while the MCU is under system reset.
    Only the DEV_ID[11:0] should be used for identification by the debugger/programmer tools */
    uint16_t cid;
    cid = (uint16_t)(DBGMCU->IDCODE) & 0x0FFF;
    return (cid);
}

/* This function reads the RDP ( Read protection option byte) value */
uint8_t get_flash_rdp_level(void)
{
    uint8_t rdp_status=0;
#if 0
    FLASH_OBProgramInitTypeDef  ob_handle;
    HAL_FLASHEx_OBGetConfig(&ob_handle);
    rdp_status = (uint8_t)ob_handle.RDPLevel;
#else
    volatile uint32_t *pOB_addr = (uint32_t*) 0x1FFFC000;
    rdp_status =  (uint8_t)(*pOB_addr >> 8) ;
#endif

    return (rdp_status);
}

/* Verify the address sent by the host */
uint8_t verify_address(uint32_t go_address)
{
    uint8_t retVal = ADDR_INVALID;
    /* So, what are the valid addresses to which we can jump ?
     * can we jump to system memory ? yes
     * can we jump to sram1 memory ?  yes
     * can we jump to sram2 memory ? yes
     * can we jump to backup sram memory ? yes
     * can we jump to peripheral memory ? its possible, but don't allow. so no
     * can we jump to external memory ? yes. */

    /** TODO: poorly written ... optimize it */
    if(go_address >= SRAM1_BASE && go_address <= SRAM1_END)
    {
        retVal = ADDR_VALID;
    }
    else if(go_address >= SRAM2_BASE && go_address <= SRAM2_END)
    {
        retVal = ADDR_VALID;
    }
    else if(go_address >= FLASH_BASE && go_address <= FLASH_END)
    {
        retVal = ADDR_VALID;
    }
    else if(go_address >= BKPSRAM_BASE && go_address <= BKPSRAM_END)
    {
        retVal = ADDR_VALID;
    }
    else
    {
        retVal = ADDR_INVALID;
    }

    return (retVal);
}

/* Perform flash erase - sector or full */
uint8_t execute_flash_erase(uint8_t sector_number, uint8_t number_of_sector)
{
    uint8_t retVal = INVALID_SECTOR;
    /* we have totally 12 sectors in STM32F407 mcu .. sector[0 to 11]
     * number_of_sector has to be in the range of 0 to 11
     * if sector_number = 0xff, that means mass erase
     * Code needs to modified if your MCU supports more flash sectors */
    FLASH_EraseInitTypeDef flashErase_handle;
    uint32_t sectorError;
    HAL_StatusTypeDef status;

    if((sector_number == 0xff ) || (sector_number < 12))
    {
        if(sector_number == (uint8_t) 0xff)
        {
            flashErase_handle.TypeErase = FLASH_TYPEERASE_MASSERASE;
        }
        else
        {
            /* Here we are just calculating how many sectors needs to erased */
            uint8_t remanining_sector = 11 - sector_number;
            if(number_of_sector > remanining_sector)
            {
                number_of_sector = remanining_sector;
            }
            flashErase_handle.TypeErase = FLASH_TYPEERASE_SECTORS;
            flashErase_handle.Sector = sector_number; /* this is the initial sector */
            flashErase_handle.NbSectors = number_of_sector;
        }
        flashErase_handle.Banks = FLASH_BANK_1;

        /* Get access to touch the flash registers */
        HAL_FLASH_Unlock();
        flashErase_handle.VoltageRange = FLASH_VOLTAGE_RANGE_3;
        status = (uint8_t) HAL_FLASHEx_Erase(&flashErase_handle, &sectorError);
        HAL_FLASH_Lock();

        retVal = status;
    }

    return (retVal);
}

/* This function writes the contents of pBuffer to  "mem_address" byte by byte
 * @note: Currently this function supports writing to Flash only.
 * @note: This functions does not check whether "mem_address" is a valid address of the flash range.
 */
uint8_t execute_mem_write(uint8_t *pBuffer, uint32_t mem_address, uint32_t len)
{
    uint8_t status = HAL_ERROR;

    /* We have to unlock flash module to get control of registers */
    HAL_FLASH_Unlock();

    for(uint32_t i = 0; i < len; i++)
    {
        /* Here we program the flash byte by byte */
        status = HAL_FLASH_Program(FLASH_TYPEPROGRAM_BYTE, mem_address + i, pBuffer[i]);
    }

    HAL_FLASH_Lock();

    return (status);
}

/* Modifying user option bytes
 * To modify the user option value, follow the sequence below:
 * 1. Check that no Flash memory operation is ongoing by checking the BSY bit in the FLASH_SR register.
 * 2. Write the desired option value in the FLASH_OPTCR register.
 * 3. Set the option start bit (OPTSTRT) in the FLASH_OPTCR register.
 * 4. Wait for the BSY bit to be cleared.
 */
uint8_t configure_flash_sector_rw_protection(uint16_t sector_details, uint8_t protection_mode, uint8_t disable)
{
    uint8_t retVal = HAL_ERROR;

    /* First configure the protection mode
     * Protection_mode = 1, means write protect of the user flash sectors
     * Protection_mode = 2, means read/write protect of the user flash sectors. Not available in STM32F4
     * According to RM of stm32f407 TABLE 9, We have to modify the address 0x1FFF C008 bit 15(SPRMOD) */

    /* Flash option control register (OPTCR) */
    volatile uint32_t *pOPTCR = (uint32_t*) 0x40023C14;

    /* Check which sectors to remove protection */
    if(disable == 1)
    {
        /* Disable all r/w protection on sectors */

        /* Option byte configuration unlock */
        HAL_FLASH_OB_Unlock();

        /* wait till no active operation on flash */
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        /* Clear the 31st bit (default state)
         * Please refer : Flash option control register (FLASH_OPTCR) in RM
         * Not needed in STM32F407 */
        /* *pOPTCR &= ~(1 << 31); */

        /* Clear the protection : make all bits belonging to sectors as 1 */
        *pOPTCR |= (0xFFF << 16);

        /* Set the option start bit (OPTSTRT) in the FLASH_OPTCR register */
        *pOPTCR |= ( 1 << 1);

        /* Wait till no active operation on flash */
        while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

        /* Option byte configuration lock */
        HAL_FLASH_OB_Lock();

        retVal = HAL_OK;
    }
    else
    {
        /* Disable specific r/w protection on sectors */

        /* MODE1 */
        if(protection_mode == (uint8_t) 1)
        {
            /* We are putting write protection on the sectors encoded in sector_details argument */

            /* Option byte configuration unlock */
            HAL_FLASH_OB_Unlock();

            /* Wait till no active operation on flash */
            while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

            /* Here we are setting just write protection for the sectors
             * Clear the 31st bit
             * Please refer : Flash option control register (FLASH_OPTCR) in RM
             * Not needed in STM32F407 */
            /* *pOPTCR &= ~(1 << 31); */

            /* Put write protection on sectors */
            *pOPTCR &= ~ (sector_details << 16);

            /* Set the option start bit (OPTSTRT) in the FLASH_OPTCR register */
            *pOPTCR |= ( 1 << 1);

            /* Wait till no active operation on flash */
            while(__HAL_FLASH_GET_FLAG(FLASH_FLAG_BSY) != RESET);

            /* Option byte configuration lock */
            HAL_FLASH_OB_Lock();

            retVal = HAL_OK;
        }
    }

    return (retVal);
}

/* Read the user option bytes related to write protection sectors */
uint16_t read_OB_rw_protection_status(void)
{
    /* This structure is given by ST Flash driver to hold the OB(Option Byte) contents */
    FLASH_OBProgramInitTypeDef OBInit;

    /* First unlock the OB(Option Byte) memory access */
    HAL_FLASH_OB_Unlock();
    /* Get the OB configuration details */
    HAL_FLASHEx_OBGetConfig(&OBInit);
    /* Lock back */
    HAL_FLASH_Lock();

    /* We are just interested in r/w protection status of the sectors */
    return ((uint16_t)OBInit.WRPSector);
}

/** Helper function to handle bootloader version command */
/**
@startuml
object bootloader_handle_getver_cmd {
  receive
  (1)length_to_follow
  (1)command_code BL_GET_VER
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (1)bl_version
}
@enduml
 */
void bootloader_handle_getver_cmd(uint8_t *bl_rx_buffer)
{
    uint8_t bl_version;

    printmsg("BL_DEBUG_MSG:bootloader_handle_getver_cmd\n\r");

    /* Total length of the command packet (6 bytes, 5 + 1) */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host (of 4 bytes) */
    uint32_t host_crc = *((uint32_t * ) (bl_rx_buffer + command_packet_len - 4) ) ;

    if (!bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(bl_rx_buffer[0], 1);
        bl_version = (uint8_t)BL_VERSION;
        printmsg("BL_DEBUG_MSG:BL_VER : %d %#x\n\r", bl_version, bl_version);
        bootloader_uart_write_data(&bl_version, 1);
    }
    else
    {
        bootloader_send_nack();
    }
}

/** Helper function to handle bootloader commands supported */
/**
@startuml
object bootloader_handle_gethelp_cmd{
  receive
  (1)length_to_follow
  (1)command_code BL_GET_HELP
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (8)bl_version
}
@enduml
 */
void bootloader_handle_gethelp_cmd(uint8_t *pBuffer)
{
    printmsg("BL_DEBUG_MSG:bootloader_handle_gethelp_cmd\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4));

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], sizeof(supported_commands));
        bootloader_uart_write_data(supported_commands, sizeof(supported_commands));

    }
    else
    {
        bootloader_send_nack();
    }
}

/** Helper function to handle chip identifier or device identifier command */
/**
@startuml
object bootloader_handle_getcid_cmd{
  receive
  (1)length_to_follow
  (1)command_code BL_GET_CID
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (2)chip_id
}
@enduml
 */
void bootloader_handle_getcid_cmd(uint8_t *pBuffer)
{
    uint16_t bl_cid_num = 0;

    printmsg("BL_DEBUG_MSG:bootloader_handle_getcid_cmd\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4));

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], 2);
        bl_cid_num = get_mcu_chip_id();
        printmsg("BL_DEBUG_MSG:MCU id : %d %#x !!\n\r", bl_cid_num, bl_cid_num);
        bootloader_uart_write_data((uint8_t *)&bl_cid_num, 2);
    }
    else
    {
        bootloader_send_nack();
    }
}

/** Helper function to handle reading the RDP ( Read protection option byte) value command */
/**
@startuml
object bootloader_handle_getrdp_cmd{
  receive
  (1)length_to_follow
  (1)command_code BL_GET_RDP_STATUS
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (1)read_protection_status
}
@enduml
 */
void bootloader_handle_getrdp_cmd(uint8_t *pBuffer)
{
    uint8_t rdp_level = 0x00;

    printmsg("BL_DEBUG_MSG:bootloader_handle_getrdp_cmd\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4));

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], 1);
        rdp_level = get_flash_rdp_level();
        printmsg("BL_DEBUG_MSG:RDP level: %d %#x\n\r", rdp_level, rdp_level);
        bootloader_uart_write_data(&rdp_level, 1);
    }
    else
    {
        bootloader_send_nack();
    }
}

/** Helper function to handle jumping to specific address command */
/**
@startuml
object bootloader_handle_go_cmd{
  receive
  (1)length_to_follow
  (1)command_code BL_GO_TO_ADDR
  ()address
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (1)status
}
@enduml
 */
void bootloader_handle_go_cmd(uint8_t *pBuffer)
{
    uint32_t go_address = 0;
    uint8_t addr_valid = ADDR_VALID;
    uint8_t addr_invalid = ADDR_INVALID;

    printmsg("BL_DEBUG_MSG:bootloader_handle_go_cmd\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4));

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], 1);

        /* Extract the go address */
        go_address = *((uint32_t *)&pBuffer[2]);
        printmsg("BL_DEBUG_MSG:GO addr: %#x\n\r", go_address);

        if(verify_address(go_address) == ADDR_VALID)
        {
            /* Tell host that address is fine */
            bootloader_uart_write_data(&addr_valid, 1);

            /* Jump to "go" address.
            we dont care what is being done there.
            host must ensure that valid code is present over there
            Its not the duty of bootloader. so just trust and jump */

            /* Not doing the below lines will result in hardfault exception for ARM cortex M */
            /* https://www.youtube.com/watch?v=VX_12SjnNhY */

            go_address |= 1; /* Make T bit = 1 */

            void (*lets_jump)(void) = (void *)go_address;

            printmsg("BL_DEBUG_MSG: jumping to go address! \n\r");

            lets_jump();
        }
        else
        {
            printmsg("BL_DEBUG_MSG:GO addr invalid ! \n\r");
            /* Tell host that address is invalid */
            bootloader_uart_write_data(&addr_invalid, 1);
        }
    }
    else
    {
        bootloader_send_nack();
    }
}

/** Helper function to erase the flash memory command */
/**
@startuml
object bootloader_handle_flash_erase_cmd{
  receive
  (1)length_to_follow
  (1)command_code BL_FLASH_ERASE
  (1)sector_number
  (1)number_of_sectors
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (1)status
}
@enduml
 */
void bootloader_handle_flash_erase_cmd(uint8_t *pBuffer)
{
    uint8_t erase_status = 0x00;

    printmsg("BL_DEBUG_MSG:bootloader_handle_flash_erase_cmd\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4)) ;

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], 1);
        printmsg("BL_DEBUG_MSG:initial_sector : %d  no_ofsectors: %d\n\r", pBuffer[2], pBuffer[3]);

        BSP_LED_On(LED4);
        erase_status = execute_flash_erase(pBuffer[2], pBuffer[3]);
        BSP_LED_Off(LED4);

        printmsg("BL_DEBUG_MSG: flash erase status: %#x\n\r", erase_status);

        bootloader_uart_write_data(&erase_status, 1);
    }
    else
    {
        bootloader_send_nack();
    }
}

/** Helper function to write data into flash memory command */
/**
@startuml
object bootloader_handle_mem_write_cmd{
  receive
  (1)length_to_follow
  (1)command_code BL_MEM_WRITE
  (4)base_memory_address
  (1)payload_length
  (x)payload
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (1)status
}
@enduml
 */
void bootloader_handle_mem_write_cmd(uint8_t *pBuffer)
{
    uint8_t write_status = 0x00;
    uint8_t payload_len = pBuffer[6];

    uint32_t mem_address = *((uint32_t *)(&pBuffer[2]));

    printmsg("BL_DEBUG_MSG:bootloader_handle_mem_write_cmd\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4));

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        printmsg("BL_DEBUG_MSG:checksum success !!\n\r");

        bootloader_send_ack(pBuffer[0], 1);

        printmsg("BL_DEBUG_MSG: mem write address : %#x\n\r", mem_address);

        if(verify_address(mem_address) == ADDR_VALID )
        {
            printmsg("BL_DEBUG_MSG: valid mem write address\n\r");

            /* Glow the led to indicate bootloader is currently writing to memory */
            BSP_LED_On(LED4);

            /* Execute mem write */
            write_status = execute_mem_write(&pBuffer[7], mem_address, payload_len);

            /* Turn off the led to indicate memory write is over */
            BSP_LED_Off(LED4);

            /* Inform host about the status */
            bootloader_uart_write_data(&write_status, 1);
        }
        else
        {
            printmsg("BL_DEBUG_MSG: invalid mem write address\n\r");
            write_status = ADDR_INVALID;
            /* Inform host that address is invalid */
            bootloader_uart_write_data(&write_status, 1);
        }
    }
    else
    {
        bootloader_send_nack();
    }
}

/** Helper function to handle modify user options R/W sectors command */
/**
@startuml
object bootloader_handle_en_rw_protect{
  receive
  (1)length_to_follow
  (1)command_code BL_EN_R_W_PROTECT
  (2)sector_details
  (1)protection_mode
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (1)status
}
@enduml
 */
void bootloader_handle_en_rw_protect(uint8_t *pBuffer)
{
    uint8_t status = 0x00;

    printmsg("BL_DEBUG_MSG:bootloader_handle_endis_rw_protect\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4));

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], 1);

        uint16_t sector_details = (pBuffer[3] << 8) | pBuffer[2];
        status = configure_flash_sector_rw_protection(sector_details, pBuffer[4], 0);

        printmsg("BL_DEBUG_MSG: flash erase status: %#x\n\r",status);

        bootloader_uart_write_data(&status, 1);
    }
    else
    {
        bootloader_send_nack();
    }
}

void bootloader_handle_mem_read (uint8_t *pBuffer)
{
    __NOP();
}

/** Helper function to handle read sector protection status command */
/**
@startuml
object bootloader_handle_read_sector_protection_status{
  receive
  (1)length_to_follow
  (1)command_code BL_READ_SECTOR_STATUS
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (2)sectors_status
}
@enduml
 */
void bootloader_handle_read_sector_protection_status(uint8_t *pBuffer)
{
    uint16_t status;
    printmsg("BL_DEBUG_MSG:bootloader_handle_read_sector_protection_status\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4)) ;

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], 2);
        status = read_OB_rw_protection_status();
        printmsg("BL_DEBUG_MSG: nWRP status: %#x\n\r", status);
        bootloader_uart_write_data((uint8_t*)&status, 2);
    }
    else
    {
        bootloader_send_nack();
    }
}

void bootloader_handle_read_otp(uint8_t *pBuffer)
{
    __NOP();
}

/** Helper function to handle disabling all sectors protection command */
/**
@startuml
object bootloader_handle_dis_rw_protect{
  receive
  (1)length_to_follow
  (1)command_code BL_DIS_R_W_PROTECT
  (4)crc\n
  return
  (1)Ok_Nok
  (1)length_to_follow
  (1)status
}
@enduml
 */
void bootloader_handle_dis_rw_protect(uint8_t *pBuffer)
{
    uint8_t status = 0x00;
    printmsg("BL_DEBUG_MSG:bootloader_handle_dis_rw_protect\n\r");

    /* Total length of the command packet */
    uint32_t command_packet_len = bl_rx_buffer[0] + 1;

    /* Extract the CRC32 sent by the Host */
    uint32_t host_crc = *((uint32_t *) (bl_rx_buffer + command_packet_len - 4));

    if(! bootloader_verify_crc(&bl_rx_buffer[0], command_packet_len - 4, host_crc))
    {
        bootloader_send_ack(pBuffer[0], 1);

        status = configure_flash_sector_rw_protection(0, 0, 1);

        printmsg("BL_DEBUG_MSG: flash erase status: %#x\n\r",status);

        bootloader_uart_write_data(&status, 1);
    }
    else
    {
        bootloader_send_nack();
    }
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  None
 * @retval None
 */
static void Error_Handler(void)
{
    /* Turn LED5 on */
    BSP_LED_On(LED5);
    while(1);
}

#ifdef  USE_FULL_ASSERT

/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
#endif
