#ifndef __STM32F4xx_LP_MODES_H
#define __STM32F4xx_LP_MODES_H

void SleepMode_Measure(void);
void StopMode_Measure(void);
void StandbyMode_Measure(void);
void StandbyRTCMode_Measure(void);
void StandbyRTCBKPSRAMMode_Measure(void);

#endif
