#include "stm32f4xx_lp_modes.h"
#include "stm32f4_discovery.h"

/**
 * @brief  This function configures the system to enter Sleep mode for
 *         current consumption measurement purpose.
 *         Sleep Mode
 *         ==========
 *            - System Running at PLL (168MHz)
 *            - Flash 3 wait state
 *            - Prefetch and Cache enabled
 *            - Code running from Internal FLASH
 *            - All peripherals disabled.
 *            - Wakeup using EXTI Line (User Button PA.01)
 * @param  None
 * @retval None
 */
void SleepMode_Measure(void)
{
	__IO uint32_t index = 0;
	GPIO_InitTypeDef GPIO_InitStructure;

	/* Configure all GPIO as analog to reduce current consumption on non used IOs */
	/* Enable GPIOs clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE , ENABLE);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* Disable GPIOs clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE, DISABLE);

	/* Configure User Button */
	UserButtom_Configurate(BUTTON_MODE_EXTI);

	/* Request to enter SLEEP mode */
	__WFI();

	/* Initialize LED4 on STM32F4-Discovery board */
	STM_EVAL_LEDInit(LED4);

	/* Infinite loop */
	while (1)
	{
		/* Toggle The LED4 */
		STM_EVAL_LEDToggle(LED4);

		/* Inserted Delay */
		for(index = 0; index < 0x5FFFFF; index++);
	}
}

/**
 * @brief  This function configures the system to enter Stop mode with RTC
 *         clocked by LSI  for current consumption measurement purpose.
 *         STOP Mode with RTC clocked by LSI
 *         =====================================
 *           - RTC Clocked by LSI
 *           - Regulator in LP mode
 *           - HSI, HSE OFF and LSI OFF if not used as RTC Clock source
 *           - No IWDG
 *           - FLASH in deep power down mode
 *           - Automatic Wakeup using RTC clocked by LSI (~20s)
 * @param  None
 * @retval None
 */
void StopMode_Measure(void)
{
	__IO uint32_t index = 0;
	GPIO_InitTypeDef GPIO_InitStructure;
	NVIC_InitTypeDef  NVIC_InitStructure;
	EXTI_InitTypeDef  EXTI_InitStructure;

	/* Allow access to RTC */
	PWR_BackupAccessCmd(ENABLE);

	/* The RTC Clock may varies due to LSI frequency dispersion. */
	/* Enable the LSI OSC */
	RCC_LSICmd(ENABLE);

	/* Wait till LSI is ready */
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);

	/* Select the RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

	/* Enable the RTC Clock */
	RCC_RTCCLKCmd(ENABLE);

	/* Wait for RTC APB registers synchronisation */
	RTC_WaitForSynchro();

	/* Configure all GPIO as analog to reduce current consumption on non used IOs */
	/* Enable GPIOs clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE , ENABLE);

	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_All;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_Init(GPIOD, &GPIO_InitStructure);
	GPIO_Init(GPIOE, &GPIO_InitStructure);
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	GPIO_Init(GPIOB, &GPIO_InitStructure);

	/* Disable GPIOs clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD | RCC_AHB1Periph_GPIOE, DISABLE);

	/* EXTI configuration */
	EXTI_ClearITPendingBit(EXTI_Line22);
	EXTI_InitStructure.EXTI_Line = EXTI_Line22;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Enable the RTC Wakeup Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = RTC_WKUP_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* RTC Wakeup Interrupt Generation: Clock Source: RTCCLK_Div16, Wakeup Time Base: ~20s
     RTC Clock Source LSI ~32KHz  
     Wakeup Time Base = (16 / (LSI)) * WakeUpCounter
	 */
	RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
	RTC_SetWakeUpCounter(0xA000-1);

	/* Enable the Wakeup Interrupt */
	RTC_ITConfig(RTC_IT_WUT, ENABLE);

	/* Enable Wakeup Counter */
	RTC_WakeUpCmd(ENABLE);

	/* FLASH Deep Power Down Mode enabled */
	PWR_FlashPowerDownCmd(ENABLE);

	/* Enter Stop Mode */
	PWR_EnterSTOPMode(PWR_Regulator_LowPower, PWR_STOPEntry_WFI);

	/* Initialize LED4 on STM32F4-Discovery board */
	STM_EVAL_LEDInit(LED4);

	/* Infinite loop */
	while (1)
	{
		/* Toggle The LED4 */
		STM_EVAL_LEDToggle(LED4);

		/* Inserted Delay */
		for(index = 0; index < 0x5FFFF; index++);
	}
}

/**
 * @brief  This function configures the system to enter Standby mode for
 *         current consumption measurement purpose.
 *         STANDBY Mode
 *         ============
 *           - Backup SRAM and RTC OFF
 *           - IWDG and LSI OFF
 *           - Wakeup using WakeUp Pin (PA.00)
 * @param  None
 * @retval None
 */
void StandbyMode_Measure(void)
{
	__IO uint32_t index = 0;

	/* Enable WKUP pin 1 */
	PWR_WakeUpPinCmd(ENABLE);

	/* Request to enter STANDBY mode (Wake Up flag is cleared in PWR_EnterSTANDBYMode function) */
	PWR_EnterSTANDBYMode();

	/* Initialize LED4 on STM32F4-Discovery board */
	STM_EVAL_LEDInit(LED4);

	/* Infinite loop */
	while (1)
	{
		/* Toggle The LED4 */
		STM_EVAL_LEDToggle(LED4);

		/* Inserted Delay */
		for(index = 0; index < 0x5FFFF; index++);
	}
}

/**
 * @brief  This function configures the system to enter Standby mode with RTC
 *         clocked by LSI for current consumption measurement purpose.
 *         STANDBY Mode with RTC clocked by LSI
 *         ========================================
 *           - RTC Clocked by LSI
 *           - IWDG OFF
 *           - Backup SRAM OFF
 *           - Automatic Wakeup using RTC clocked by LSI (after ~20s)
 * @param  None
 * @retval None
 */
void StandbyRTCMode_Measure(void)
{
	__IO uint32_t index = 0;

	/* Allow access to RTC */
	PWR_BackupAccessCmd(ENABLE);

	/* The RTC Clock may varies due to LSI frequency dispersion. */
	/* Enable the LSI OSC */
	RCC_LSICmd(ENABLE);

	/* Wait till LSI is ready */
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);

	/* Select the RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

	/* Enable the RTC Clock */
	RCC_RTCCLKCmd(ENABLE);

	/* Wait for RTC APB registers synchronisation */
	RTC_WaitForSynchro();

	/* RTC Wakeup Interrupt Generation: Clock Source: RTCCLK_Div16, Wakeup Time Base: ~20s
     RTC Clock Source LSI ~32KHz 

     Wakeup Time Base = (16 / (LSI)) * WakeUpCounter
	 */
	RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
	RTC_SetWakeUpCounter(0xA000-1);

	/* Enable the Wakeup Interrupt */
	RTC_ITConfig(RTC_IT_WUT, ENABLE);

	/* Enable Wakeup Counter */
	RTC_WakeUpCmd(ENABLE);

	/* Clear WakeUp (WUTF) pending flag */
	RTC_ClearFlag(RTC_FLAG_WUTF);

	/* Request to enter STANDBY mode (Wake Up flag is cleared in PWR_EnterSTANDBYMode function) */
	PWR_EnterSTANDBYMode();

	/* Initialize LED4 on STM32F4-Discovery board */
	STM_EVAL_LEDInit(LED4);

	/* Infinite loop */
	while (1)
	{
		/* Toggle The LED4 */
		STM_EVAL_LEDToggle(LED4);

		/* Inserted Delay */
		for(index = 0; index < 0x5FFFF; index++);
	}
}

/**
 * @brief  This function configures the system to enter Standby mode with RTC
 *         clocked by LSI and with Backup SRAM ON for current consumption
 *         measurement purpose.
 *         STANDBY Mode with RTC clocked by LSI and BKPSRAM
 *         ====================================================
 *           - RTC Clocked by LSI
 *           - Backup SRAM ON
 *           - IWDG OFF
 *           - Automatic Wakeup using RTC clocked by LSI (after ~20s)
 * @param  None
 * @retval None
 */
void StandbyRTCBKPSRAMMode_Measure(void)
{
	__IO uint32_t index = 0;

	/* Allow access to RTC */
	PWR_BackupAccessCmd(ENABLE);

	/* The RTC Clock may varies due to LSI frequency dispersion. */
	/* Enable the LSI OSC */
	RCC_LSICmd(ENABLE);

	/* Wait till LSI is ready */
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET);

	/* Select the RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

	/* Enable the RTC Clock */
	RCC_RTCCLKCmd(ENABLE);

	/* Wait for RTC APB registers synchronisation */
	RTC_WaitForSynchro();

	/*  Backup SRAM */
	/* Enable BKPRAM Clock */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_BKPSRAM, ENABLE);

	/* Enable the Backup SRAM low power Regulator */
	PWR_BackupRegulatorCmd(ENABLE);

	/* Wait until the Backup SRAM low power Regulator is ready */
	while(PWR_GetFlagStatus(PWR_FLAG_BRR) == RESET);

	/* RTC Wakeup Interrupt Generation: Clock Source: RTCCLK_Div16, Wakeup Time Base: ~20s
     RTC Clock Source LSI ~32KHz  

     Wakeup Time Base = (16 / (LSI)) * WakeUpCounter
	 */
	RTC_WakeUpClockConfig(RTC_WakeUpClock_RTCCLK_Div16);
	RTC_SetWakeUpCounter(0xA000-1);

	/* Enable the Wakeup Interrupt */
	RTC_ITConfig(RTC_IT_WUT, ENABLE);

	/* Enable Wakeup Counter */
	RTC_WakeUpCmd(ENABLE);

	/* Clear WakeUp (WUTF) pending flag */
	RTC_ClearFlag(RTC_FLAG_WUTF);

	/* Request to enter STANDBY mode (Wake Up flag is cleared in PWR_EnterSTANDBYMode function) */
	PWR_EnterSTANDBYMode();

	/* Initialize LED4 on STM32F4-Discovery board */
	STM_EVAL_LEDInit(LED4);

	/* Infinite loop */
	while (1)
	{
		/* Toggle The LED4 */
		STM_EVAL_LEDToggle(LED4);

		/* Inserted Delay */
		for(index = 0; index < 0x5FFFF; index++);
	}
}
