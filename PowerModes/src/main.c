#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "stm32f4xx_lp_modes.h"

void SetSysClockFrecuency(void);

#if !defined (SLEEP_MODE) && !defined (STOP_MODE) && !defined (STANDBY_MODE) && !defined (STANDBY_RTC_MODE) && !defined (STANDBY_RTC_BKPSRAM_MODE)
//#define SLEEP_MODE
//#define STOP_MODE
#define STANDBY_MODE
//#define STANDBY_RTC_MODE
//#define STANDBY_RTC_BKPSRAM_MODE
#endif

//NO MOVER A LAS SIGUIENTES 3 DEFINICIONES.
#define _HSI	0
#define _HSE	1
#define _PLL	2

//ESCALADORES DEL PLL.
#define PLL_M			8
#define PLL_N			168
#define PLL_P			2
#define PLL_Q			7
#define PLLI2S_N		192
#define PLLI2S_R		5

//SELECCION DE LOS MULTIPLEXORES.
#define PLLSourceMux	_HSE	//Valores posibles: _HSI y _HSE
#define SystemClockMux	_HSI	//Valores posibles: _HSI, _HSE y _PLL

int main(void)
{
	//Frecuencia 16MHz
	SetSysClockFrecuency();

	/* Enable PWR APB1 Clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

	/* Allow access to Backup */
	PWR_BackupAccessCmd(ENABLE);

	/* Reset RTC Domain */
	RCC_BackupResetCmd(ENABLE);
	RCC_BackupResetCmd(DISABLE);

	/* Configure User Button */
	UserButtom_Configurate(BUTTON_MODE_GPIO);

	STM_EVAL_LEDInit(LED6);

	STM_EVAL_LEDOn(LED6);
	/* Wait until User button is pressed to enter the Low Power mode */
	/* Loop while User button is maintained pressed */
	while(UserButtom_Read() == SET);
	while(UserButtom_Read() == RESET);
	STM_EVAL_LEDOff(LED6);

#if defined (SLEEP_MODE)
	/* Sleep Mode Entry
      - System Running at PLL (168MHz)
      - Flash 3 wait state
      - Prefetch and Cache enabled
      - Code running from Internal FLASH
      - All peripherals disabled.
      - Wakeup using EXTI Line (User Button PA.01)
	 */
	SleepMode_Measure();

#elif defined (STOP_MODE)
	/* STOP Mode Entry
      - RTC Clocked by LSI
      - Regulator in LP mode
      - HSI, HSE OFF and LSI OFF if not used as RTC Clock source  
      - No IWDG
      - FLASH in deep power down mode
      - Automatic Wakeup using RTC clocked by LSI (after ~20s)
	 */
	StopMode_Measure();

#elif defined (STANDBY_MODE)
	/* STANDBY Mode Entry
      - Backup SRAM and RTC OFF
      - IWDG and LSI OFF
      - Wakeup using WakeUp Pin (PA.00)
	 */
	StandbyMode_Measure();

#elif defined (STANDBY_RTC_MODE)
	/* STANDBY Mode with RTC on LSI Entry
      - RTC Clocked by LSI
      - IWDG OFF and LSI OFF if not used as RTC Clock source
      - Backup SRAM OFF
      - Automatic Wakeup using RTC clocked by LSI (after ~20s)
	 */
	StandbyRTCMode_Measure();

#elif defined (STANDBY_RTC_BKPSRAM_MODE)
	/* STANDBY Mode with RTC on LSI Entry
      - RTC Clocked by LSI
      - Backup SRAM ON
      - IWDG OFF
      - Automatic Wakeup using RTC clocked by LSI (after ~20s)
	 */
	StandbyRTCBKPSRAMMode_Measure();

#else
	volatile uint32_t i = 0;

	/* Initialize LED4 on STM32F4-Discovery board */
	STM_EVAL_LEDInit(LED4);

	/* Infinite loop */
	while (1)
	{
		/* Toggle The LED4 */
		STM_EVAL_LEDToggle(LED4);

		/* Inserted Delay */
		for(i = 0; i < 0x5FF; i++);
	}
#endif
}

void SetSysClockFrecuency(void)
{
	volatile uint32_t StartUpCounter = 0, HSEStatus = 0;

	/* Enable HSE */
	RCC->CR |= ((uint32_t)RCC_CR_HSEON);

	/* Wait till HSE is ready and if Time out is reached exit */
	do
	{
		HSEStatus = RCC->CR & RCC_CR_HSERDY;
		StartUpCounter++;
	} while((HSEStatus == 0) && (StartUpCounter != HSE_STARTUP_TIMEOUT));

	if ((RCC->CR & RCC_CR_HSERDY) != RESET)
	{
		HSEStatus = (uint32_t)0x01;
	}
	else
	{
		HSEStatus = (uint32_t)0x00;
	}

	if (HSEStatus == (uint32_t)0x01)
	{
		/* Enable high performance mode, System frequency up to 168 MHz */
		RCC->APB1ENR |= RCC_APB1ENR_PWREN;
		PWR->CR |= PWR_CR_PMODE;

		/* HCLK = SYSCLK / 1*/
		RCC->CFGR |= RCC_CFGR_HPRE_DIV1;	//Arduino: AHB preescaler

		/* PCLK2 = HCLK / 2*/
		RCC->CFGR |= RCC_CFGR_PPRE2_DIV2;	//Arduino: APB2 preescaler

		/* PCLK1 = HCLK / 4*/
		RCC->CFGR |= RCC_CFGR_PPRE1_DIV4;	//Arduino: APB1 preescaler

		/* Configure the main PLL */
		if(PLLSourceMux == _HSI)
		{
			RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) -1) << 16) | (RCC_PLLCFGR_PLLSRC_HSI) | (PLL_Q << 24);
		}
		else if(PLLSourceMux == _HSE)
		{
			RCC->PLLCFGR = PLL_M | (PLL_N << 6) | (((PLL_P >> 1) -1) << 16) | (RCC_PLLCFGR_PLLSRC_HSE) | (PLL_Q << 24);
		}
		else
		{
			/*
			 * DEFAULT
			 * PLLSourceMux: HSI
			 * PLL_M: 8
			 * PLL_N: 168
			 * PLL_P: 2
			 * PLL_Q: 7
			*/
			RCC->PLLCFGR = 8 | (168 << 6) | (((2 >> 1) -1) << 16) | (RCC_PLLCFGR_PLLSRC_HSI) | (7 << 24);
		}


		/* Enable the main PLL */
		RCC->CR |= RCC_CR_PLLON;

		/* Wait till the main PLL is ready */
		while((RCC->CR & RCC_CR_PLLRDY) == 0);

		/* Configure Flash prefetch, Instruction cache, Data cache and wait state */
		FLASH->ACR = FLASH_ACR_ICEN |FLASH_ACR_DCEN |FLASH_ACR_LATENCY_5WS;

		/* Select the system clock source */
		RCC->CFGR &= (uint32_t)((uint32_t)~(RCC_CFGR_SW));
		if(SystemClockMux == _HSI)
		{
			RCC->CFGR |= RCC_CFGR_SW_HSI;
			while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_HSI); /* Wait till the HSI is used as system clock source */
		}
		else if(SystemClockMux == _HSE)
		{
			RCC->CFGR |= RCC_CFGR_SW_HSE;
			while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_HSE);	/* Wait till the HSE is used as system clock source */
		}
		else if(SystemClockMux == _PLL)
		{
			RCC->CFGR |= RCC_CFGR_SW_PLL;
			while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_PLL);	/* Wait till the HSE is used as system clock source */
		}
		else
		{
			/*
			 * DEFAULT: HSI as sysclock.
			*/
			RCC->CFGR |= RCC_CFGR_SW_HSI;
			while ((RCC->CFGR & (uint32_t)RCC_CFGR_SWS ) != RCC_CFGR_SWS_HSI); /* Wait till the HSI is used as system clock source */
		}
	}
	else
	{
		while(1);
	}

	/* PLLI2S clock used as I2S clock source */
	RCC->CFGR &= ~RCC_CFGR_I2SSRC;

	/* Configure PLLI2S */
	RCC->PLLI2SCFGR = (PLLI2S_N << 6) | (PLLI2S_R << 28);

	/* Enable PLLI2S */
	RCC->CR |= ((uint32_t)RCC_CR_PLLI2SON);

	/* Wait till PLLI2S is ready */
	while((RCC->CR & RCC_CR_PLLI2SRDY) == 0);
}

void EXTI1_IRQHandler(void)
{
	/* Make sure that interrupt flag is set */
	if (EXTI_GetITStatus(EXTI_Line1) != RESET)
	{
		/* Clear interrupt flag */
		EXTI_ClearITPendingBit(EXTI_Line1);
	}
}

void RTC_WKUP_IRQHandler(void)
{
	if(RTC_GetITStatus(RTC_IT_WUT) != RESET)
	{
		RTC_ClearITPendingBit(RTC_IT_WUT);
		EXTI_ClearITPendingBit(EXTI_Line22);
	}
}

