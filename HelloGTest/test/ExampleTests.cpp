#include "../inc/Testing.hpp"

#include <gtest/gtest.h>

TEST(Testing, Edad) {
    
    Family familia0 = Family();
    Family familia1 = Family(20);

    familia0.setEdad(10);
    int edad0 = familia0.getEdad();
    int edad1 = familia1.getEdad();

    EXPECT_EQ(10, edad0);
    EXPECT_EQ(20, edad1);
}