/**
 ******************************************************************************
 * @file    main.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    18-September-2020
 * @brief   Default main function.
 ******************************************************************************
 */

#include "../inc/UnitTest.hpp"
#ifndef UNIT_TEST
#include "stm32f4xx.h"
#else
#include <cstdint>
#endif

#include "../inc/Testing.hpp"

int main(void)
{
	Family familia1 = Family();
	Family familia2 = Family(20);

	int edadFamilia1 = familia1.getEdad();
	int edadFamilia2 = familia2.getEdad();

	for(;;);
}
