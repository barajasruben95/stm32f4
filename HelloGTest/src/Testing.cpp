/**
 ******************************************************************************
 * @file    Testing.cpp
 * @author  Ruben Barajas
 * @version V1.0
 * @date    18-September-2020
 * @brief   Testing only
 ******************************************************************************
 */

#include "../inc/Testing.hpp"

Family::Family()
{
	edad = 10;
}

Family::Family(int edad)
{
	this->edad = edad;
}

Family::~Family()
{
	//Destructor was called
}

void Family::setEdad(int edad)
{
	this->edad = edad;
}

uint8_t Family::getEdad()
{
	return edad;
}
