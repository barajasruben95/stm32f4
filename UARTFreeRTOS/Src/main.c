#include "main.h"
#include "stm32f4xx_hal.h"
#include "cmsis_os.h"

#define _Baudrate (pdMS_TO_TICKS(13UL)) //110bps. Teraterm tiene una ligera falla con tasa de 110bps.
#define _SamplingBit (3*_Baudrate)/2

void SystemClock_Config(void);
void LEDs_Config(void);
void Pin_Config(GPIO_TypeDef *port, uint16_t gpio_pin);
void ExternalInterrupt_Config(GPIO_TypeDef *port, uint16_t gpio_pin, uint8_t subpriority);
void StartDefaultTask(void const * argument);

//Alarmas que verifican las UARTs.
void UART_0(TimerHandle_t xTimer);
void UART_1(TimerHandle_t xTimer);
void UART_2(TimerHandle_t xTimer);
void UART_3(TimerHandle_t xTimer);

//Tareas que sacan el PWM.
void PWM0( void *pvParameters );
void PWM1( void *pvParameters );
void PWM2( void *pvParameters );
void PWM3( void *pvParameters );

//FreeRTOS
osThreadId defaultTaskHandle;
TimerHandle_t alarmUART0, alarmUART1, alarmUART2, alarmUART3;

//Ciclos de trabajo.
uint8_t dutyCyclePWM0 = 5;
uint8_t dutyCyclePWM1 = 25;
uint8_t dutyCyclePWM2 = 65;
uint8_t dutyCyclePWM3 = 95;

//Recepcion de datos UART0.
uint8_t _DataRxUART0;
uint8_t _CantidadBitsRxUART0 = 0;
uint8_t _BufferRxUART0[BUFFER_RX_UART0];
uint8_t _CantidadDatosRxUART0 = 0;

//Recepcion de datos UART1.
uint8_t _DataRxUART1;
uint8_t _CantidadBitsRxUART1 = 0;
uint8_t _BufferRxUART1[BUFFER_RX_UART1];
uint8_t _CantidadDatosRxUART1 = 0;

//Recepcion de datos UART2.
uint8_t _DataRxUART2;
uint8_t _CantidadBitsRxUART2 = 0;
uint8_t _BufferRxUART2[BUFFER_RX_UART2];
uint8_t _CantidadDatosRxUART2 = 0;

//Recepcion de datos UART3.
uint8_t _DataRxUART3;
uint8_t _CantidadBitsRxUART3 = 0;
uint8_t _BufferRxUART3[BUFFER_RX_UART3];
uint8_t _CantidadDatosRxUART3 = 0;

int main(void)
{
	/* Initialize HAL libraries */
	HAL_Init();

	/* Configure the system clock */
	SystemClock_Config();

	/* Initialize LEDs */
	LEDs_Config();

	/* Initialize pin PWM */
	Pin_Config(Port_PWM, Pin_PWM0);
	Pin_Config(Port_PWM, Pin_PWM1);
	Pin_Config(Port_PWM, Pin_PWM2);
	Pin_Config(Port_PWM, Pin_PWM3);

	/* Configure external interrupt UART RX */
	ExternalInterrupt_Config(Port_UART, Pin_UART0, 5);
	ExternalInterrupt_Config(Port_UART, Pin_UART1, 6);
	ExternalInterrupt_Config(Port_UART, Pin_UART2, 7);
	ExternalInterrupt_Config(Port_UART, Pin_UART3, 8);

	/* Create the thread(s) */
	osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
	defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

	/* Create tasks for PWMs */
	xTaskCreate(PWM0, "PWM 0", configMINIMAL_STACK_SIZE, NULL, 0, NULL );
	xTaskCreate(PWM1, "PWM 1", configMINIMAL_STACK_SIZE, NULL, 1, NULL );
	xTaskCreate(PWM2, "PWM 2", configMINIMAL_STACK_SIZE, NULL, 2, NULL );
	xTaskCreate(PWM3, "PWM 3", configMINIMAL_STACK_SIZE, NULL, 3, NULL );

	/* Create alarms for UARTs */
	alarmUART0 = xTimerCreate("Alarm 0", _SamplingBit, pdTRUE, 0, UART_0);
	alarmUART1 = xTimerCreate("Alarm 1", _SamplingBit, pdTRUE, 0, UART_1);
	alarmUART2 = xTimerCreate("Alarm 2", _SamplingBit, pdTRUE, 0, UART_2);
	alarmUART3 = xTimerCreate("Alarm 3", _SamplingBit, pdTRUE, 0, UART_3);

	/* Start scheduler */
	osKernelStart();

	/* We should never get here as control is now taken by the scheduler */
	while (1);
}

//**********************
// *** PWM 0 ***
//**********************
void PWM0 (void *pvParameters )
{
	const TickType_t frecuenciaPWM0 = pdMS_TO_TICKS(250UL);
	TickType_t PWM0;

	for(;;)
	{
		PWM0 = (dutyCyclePWM0*frecuenciaPWM0)/100;
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM0, GPIO_PIN_SET);
		vTaskDelay(PWM0);
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM0, GPIO_PIN_RESET);
		vTaskDelay(frecuenciaPWM0 - PWM0);
	}
}

//**********************
// *** PWM 1 ***
//**********************
void PWM1 (void *pvParameters )
{
	const TickType_t frecuenciaPWM1 = pdMS_TO_TICKS(250UL);
	TickType_t PWM1;

	for(;;)
	{
		PWM1 = (dutyCyclePWM1*frecuenciaPWM1)/100;
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM1, GPIO_PIN_SET);
		vTaskDelay(PWM1);
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM1, GPIO_PIN_RESET);
		vTaskDelay(frecuenciaPWM1 - PWM1);
	}
}

//**********************
// *** PWM 2 ***
//**********************
void PWM2 (void *pvParameters )
{
	const TickType_t frecuenciaPWM2 = pdMS_TO_TICKS(250UL);
	TickType_t PWM2;

	for(;;)
	{
		PWM2 = (dutyCyclePWM2*frecuenciaPWM2)/100;
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM2, GPIO_PIN_SET);
		vTaskDelay(PWM2);
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM2, GPIO_PIN_RESET);
		vTaskDelay(frecuenciaPWM2 - PWM2);
	}
}

//**********************
// *** PWM 3 ***
//**********************
void PWM3 (void *pvParameters )
{
	const TickType_t frecuenciaPWM3 = pdMS_TO_TICKS(250UL);
	TickType_t PWM3;

	for(;;)
	{
		PWM3 = (dutyCyclePWM3*frecuenciaPWM3)/100;
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM3, GPIO_PIN_SET);
		vTaskDelay(PWM3);
		HAL_GPIO_WritePin(Port_PWM, Pin_PWM3, GPIO_PIN_RESET);
		vTaskDelay(frecuenciaPWM3 - PWM3);
	}
}

//**********************
// *** ALARMA UART 0 ***
//**********************
void UART_0( TimerHandle_t xTimer )
{
	if(_CantidadBitsRxUART0 < 8)
	{
		_DataRxUART0 >>= 1;
		if((Port_UART->IDR & Pin_UART0) != 0)
			_DataRxUART0 |= 0x80;
		if(0 == _CantidadBitsRxUART0++)
		{
			if( xTimerChangePeriod( alarmUART0, _Baudrate, 20 ) != pdPASS )
				while(1);
		}
	}
	else
	{
		_BufferRxUART0[_CantidadDatosRxUART0++%BUFFER_RX_UART0] = _DataRxUART0;	//Guardar dato en el buffer de recepcion.
		_CantidadBitsRxUART0 = 0;
		if( xTimerChangePeriod( alarmUART0, _SamplingBit, 20 ) != pdPASS )
			while(1);
		if( xTimerStop( alarmUART0, 20 ) != pdPASS )
			while(1);
		//Colocar nuevo ciclo de trabajo.
		if(_BufferRxUART0[0]>100)
			_BufferRxUART0[0]=100;
		dutyCyclePWM0 = _BufferRxUART0[0];
		//Habilitar interrupcion para el start bit de otra transmision.
		HAL_NVIC_EnableIRQ(EXTI0_IRQn);
	}
}

//**********************
// *** ALARMA UART 1 ***
//**********************
void UART_1( TimerHandle_t xTimer )
{
	if(_CantidadBitsRxUART1 < 8)
	{
		_DataRxUART1 >>= 1;
		if((Port_UART->IDR & Pin_UART1) != 0)
			_DataRxUART1 |= 0x80;
		if(0 == _CantidadBitsRxUART1++)
		{
			if( xTimerChangePeriod( alarmUART1, _Baudrate, 20 ) != pdPASS )
				while(1);
		}
	}
	else
	{
		_BufferRxUART1[_CantidadDatosRxUART1++%BUFFER_RX_UART1] = _DataRxUART1;	//Guardar dato en el buffer de recepcion.
		_CantidadBitsRxUART1 = 0;
		if( xTimerChangePeriod( alarmUART1, _SamplingBit, 20 ) != pdPASS )
			while(1);
		if( xTimerStop( alarmUART1, 20 ) != pdPASS )
			while(1);
		//Colocar nuevo ciclo de trabajo.
		if(_BufferRxUART1[0]>100)
			_BufferRxUART1[0]=100;
		dutyCyclePWM1 = _BufferRxUART1[0];
		//Habilitar interrupcion para el start bit de otra transmision.
		HAL_NVIC_EnableIRQ(EXTI1_IRQn);
	}
}

//**********************
// *** ALARMA UART 2 ***
//**********************
void UART_2( TimerHandle_t xTimer )
{
	if(_CantidadBitsRxUART2 < 8)
	{
		_DataRxUART2 >>= 1;
		if((Port_UART->IDR & Pin_UART2) != 0)
			_DataRxUART2 |= 0x80;
		if(0 == _CantidadBitsRxUART2++)
		{
			if( xTimerChangePeriod( alarmUART2, _Baudrate, 20 ) != pdPASS )
				while(1);
		}
	}
	else
	{
		_BufferRxUART2[_CantidadDatosRxUART2++%BUFFER_RX_UART2] = _DataRxUART2;	//Guardar dato en el buffer de recepcion.
		_CantidadBitsRxUART2 = 0;
		if( xTimerChangePeriod( alarmUART2, _SamplingBit, 20 ) != pdPASS )
			while(1);
		if( xTimerStop( alarmUART2, 20 ) != pdPASS )
			while(1);
		//Colocar nuevo ciclo de trabajo.
		if(_BufferRxUART2[0]>100)
			_BufferRxUART2[0]=100;
		dutyCyclePWM2 = _BufferRxUART2[0];
		//Habilitar interrupcion para el start bit de otra transmision.
		HAL_NVIC_EnableIRQ(EXTI2_IRQn);
	}
}

//**********************
// *** ALARMA UART 3 ***
//**********************
void UART_3( TimerHandle_t xTimer )
{
	if(_CantidadBitsRxUART3 < 8)
	{
		_DataRxUART3 >>= 1;
		if((Port_UART->IDR & Pin_UART3) != 0)
			_DataRxUART3 |= 0x80;
		if(0 == _CantidadBitsRxUART3++)
		{
			if( xTimerChangePeriod( alarmUART3, _Baudrate, 20 ) != pdPASS )
				while(1);
		}
	}
	else
	{
		_BufferRxUART3[_CantidadDatosRxUART3++%BUFFER_RX_UART3] = _DataRxUART3;	//Guardar dato en el buffer de recepcion.
		_CantidadBitsRxUART3 = 0;
		if( xTimerChangePeriod( alarmUART3, _SamplingBit, 20 ) != pdPASS )
			while(1);
		if( xTimerStop( alarmUART3, 20 ) != pdPASS )
			while(1);
		//Colocar nuevo ciclo de trabajo.
		if(_BufferRxUART3[0]>100)
			_BufferRxUART3[0]=100;
		dutyCyclePWM3 = _BufferRxUART3[0];
		//Habilitar interrupcion para el start bit de otra transmision.
		HAL_NVIC_EnableIRQ(EXTI3_IRQn);
	}
}

//*************************************************************************************************
void SystemClock_Config(void)
{
	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage */
	__HAL_RCC_PWR_CLK_ENABLE();

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	/**Initializes the CPU, AHB and APB busses clocks */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 336;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 7;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
	{
		_Error_Handler(__FILE__, __LINE__);
	}
}

//*************************************************************************************************
void LEDs_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* GPIO Port Clock Enable */
	__HAL_RCC_GPIOD_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOD, LD3_Pin | LD4_Pin | LD5_Pin | LD6_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pins : LD3_Pin LD4_Pin LD5_Pin LD6_Pin */
	GPIO_InitStruct.Pin = LD3_Pin | LD4_Pin | LD5_Pin | LD6_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}

//*************************************************************************************************
void Pin_Config(GPIO_TypeDef *port, uint16_t gpio_pin)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/* Enable GPIO clock */
	if(port == GPIOA)
		__HAL_RCC_GPIOA_CLK_ENABLE();
	else if(port == GPIOB)
		__HAL_RCC_GPIOB_CLK_ENABLE();
	else if(port == GPIOC)
		__HAL_RCC_GPIOC_CLK_ENABLE();
	else if(port == GPIOD)
		__HAL_RCC_GPIOD_CLK_ENABLE();
	else if(port == GPIOE)
		__HAL_RCC_GPIOE_CLK_ENABLE();
	else if(port == GPIOF)
		__HAL_RCC_GPIOF_CLK_ENABLE();
	else if(port == GPIOG)
		__HAL_RCC_GPIOG_CLK_ENABLE();
	else
	{
		__HAL_RCC_GPIOB_CLK_ENABLE();	//Puerto por default.
		port = GPIOB;
	}

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(port, gpio_pin, GPIO_PIN_SET);

	/*Configure pin */
	GPIO_InitStruct.Pin = gpio_pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_HIGH;
	HAL_GPIO_Init(port, &GPIO_InitStruct);
}

//*************************************************************************************************
void ExternalInterrupt_Config(GPIO_TypeDef *port, uint16_t gpio_pin, uint8_t subpriority)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	uint8_t externalInterrupt;

	/* Enable GPIO clock */
	if(port == GPIOA)
		__HAL_RCC_GPIOA_CLK_ENABLE();
	else if(port == GPIOB)
		__HAL_RCC_GPIOB_CLK_ENABLE();
	else if(port == GPIOC)
		__HAL_RCC_GPIOC_CLK_ENABLE();
	else if(port == GPIOD)
		__HAL_RCC_GPIOD_CLK_ENABLE();
	else if(port == GPIOE)
		__HAL_RCC_GPIOE_CLK_ENABLE();
	else if(port == GPIOF)
		__HAL_RCC_GPIOF_CLK_ENABLE();
	else if(port == GPIOG)
		__HAL_RCC_GPIOG_CLK_ENABLE();
	else
	{
		__HAL_RCC_GPIOA_CLK_ENABLE();	//GPIOA por default.
		port = GPIOA;
	}

	/* Configure pin as input floating */
	GPIO_InitStructure.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStructure.Pull = GPIO_NOPULL;
	GPIO_InitStructure.Pin = gpio_pin;
	HAL_GPIO_Init(port, &GPIO_InitStructure);

	/* Enable and set EXTI Line0 Interrupt priority */
	if (gpio_pin == GPIO_PIN_0)
		externalInterrupt = EXTI0_IRQn;
	else if(gpio_pin == GPIO_PIN_1)
		externalInterrupt = EXTI1_IRQn;
	else if(gpio_pin == GPIO_PIN_2)
		externalInterrupt = EXTI2_IRQn;
	else if(gpio_pin == GPIO_PIN_3)
		externalInterrupt = EXTI3_IRQn;
	else if(gpio_pin == GPIO_PIN_4)
		externalInterrupt = EXTI4_IRQn;
	else
		externalInterrupt = EXTI0_IRQn; //Pin0 por default.

	HAL_NVIC_SetPriority(externalInterrupt, 5, subpriority);
	HAL_NVIC_EnableIRQ(externalInterrupt);
}

//*************************************************************************************************
void EXTI0_IRQHandler(void)
{
	//Limpiar bandera interrupcion externa.
	HAL_GPIO_EXTI_IRQHandler(Pin_UART0);
	//Deshabilitar interrupciones externas pin.
	HAL_NVIC_DisableIRQ(EXTI0_IRQn);
	//Arrancar alarma.
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	if( xTimerStartFromISR( alarmUART0, &xHigherPriorityTaskWoken ) != pdPASS )
		while(1);
	if( xHigherPriorityTaskWoken != pdFALSE )
		asm("nop");
}

//*************************************************************************************************
void EXTI1_IRQHandler(void)
{
	//Limpiar bandera interrupcion externa.
	HAL_GPIO_EXTI_IRQHandler(Pin_UART1);
	//Deshabilitar interrupciones externas pin.
	HAL_NVIC_DisableIRQ(EXTI1_IRQn);
	//Arrancar alarma.
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	if( xTimerStartFromISR( alarmUART1, &xHigherPriorityTaskWoken ) != pdPASS )
		while(1);
	if( xHigherPriorityTaskWoken != pdFALSE )
		asm("nop");
}

//*************************************************************************************************
void EXTI2_IRQHandler(void)
{
	//Limpiar bandera interrupcion externa.
	HAL_GPIO_EXTI_IRQHandler(Pin_UART2);
	//Deshabilitar interrupciones externas pin.
	HAL_NVIC_DisableIRQ(EXTI2_IRQn);
	//Arrancar alarma.
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	if( xTimerStartFromISR( alarmUART2, &xHigherPriorityTaskWoken ) != pdPASS )
		while(1);
	if( xHigherPriorityTaskWoken != pdFALSE )
		asm("nop");
}

//*************************************************************************************************
void EXTI3_IRQHandler(void)
{
	//Limpiar bandera interrupcion externa.
	HAL_GPIO_EXTI_IRQHandler(Pin_UART3);
	//Deshabilitar interrupciones externas pin.
	HAL_NVIC_DisableIRQ(EXTI3_IRQn);
	//Arrancar alarma.
	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	if( xTimerStartFromISR( alarmUART3, &xHigherPriorityTaskWoken ) != pdPASS )
		while(1);
	if( xHigherPriorityTaskWoken != pdFALSE )
		asm("nop");
}

/**
 * @brief  Function implementing the defaultTask thread.
 * @param  argument: Not used
 * @retval None
 */
void StartDefaultTask(void const * argument)
{
	for(;;)
	{
		osDelay(1);
	}
}

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line)
{
	while(1);
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
