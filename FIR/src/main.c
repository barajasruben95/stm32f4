#include "stm32f4xx_conf.h"
#include "UART.h"
#include "arm_math.h"

#define BUFFERAddreLeng     	9
#define BUFFER_SIZE				(1<<BUFFERAddreLeng)	//Tama�o de datos: 512
#define FFT_SIZE				BUFFER_SIZE/2			//Tama�o FFT: 256
#define SAMPLING_RATE			44000					//Frecuencia de muestreo

#define ADC3_DR_ADDRESS    		(uint32_t)(&(ADC3->DR))

volatile uint16_t ADCMem[BUFFER_SIZE*2];

volatile uint8_t CurrMem = 0;
volatile uint8_t InUseMem = 0;
volatile uint8_t ADCNewBlock = 0;

void ADC3_CH12_DMA_Config(void);
void TIM_Config(void);

int main(void)
{
	float32_t fftInput[BUFFER_SIZE];		//Datos a procesar por la FFT
	float32_t fft_Real[FFT_SIZE];			//Cortar a la mitad los datos de la FFT (espejo)
	float32_t fft_Magnitud[FFT_SIZE/2];		//Magnitud de la FFT cortada.
	q15_t Temp[BUFFER_SIZE];
	uint32_t freq;							//Valor de la frecuencia.
	uint16_t i;
	uint8_t prueba[] = {"PRUEBA\n\r"};

	float32_t maxValue;			//Valor maximo de la FFT.
	uint32_t maxIndex;			//Index del arreglo de entrada donde se encuentra el valor maximo.

	UART__Init();
	UART__WriteString(&prueba[0]);

	//Configure Tim2 and ADC3 with DMA
	TIM_Config();
	ADC3_CH12_DMA_Config();

	/* Enable ADC and DMA */
	ADC_Cmd(ADC3, ENABLE);
	ADC_DMACmd(ADC3, ENABLE);

	while (1)
	{
		while(!ADCNewBlock);
		ADCNewBlock = 0;
		InUseMem = 1;

		//uint16_t (with 12 bits ADC) -> Q15
		for(i=0;i<BUFFER_SIZE;i++)
		{
			Temp[i]=(q15_t)(ADCMem[i+(CurrMem<<BUFFERAddreLeng)]-2048);
		}

		//Q15 -> float
		arm_q15_to_float((q15_t *)&Temp, (float32_t *)&fftInput, (uint32_t)BUFFER_SIZE);

		//Inicializar FFT.
		arm_cfft_radix4_instance_f32 S;
		if(arm_cfft_radix4_init_f32(&S, (uint16_t)FFT_SIZE, (uint8_t)0, (uint8_t)1) != ARM_MATH_SUCCESS)
			while(1);

		//Desarrollar FFT.
		arm_cfft_radix4_f32(&S, (float32_t *)&fftInput);

		for(i = 0; i < FFT_SIZE; i++)
		{
			fft_Real[i] = fftInput[i];
		}

		//Magnitud.
		arm_cmplx_mag_f32((float32_t *)&fft_Real, (float32_t *)&fft_Magnitud, FFT_SIZE);

		//Quitar componente de DC.
		fft_Magnitud[0] = 0;

		//Calcular maximo.
		arm_max_f32((float32_t *)&fft_Magnitud, (uint32_t)FFT_SIZE/2, (float32_t *)&maxValue, (uint32_t *)&maxIndex);

		//Calcular la frecuencia de la se�al.
		freq = (SAMPLING_RATE/BUFFER_SIZE)*(maxIndex+1);

		//Enviar los datos por serial.
		UART__WriteNumber(freq, DECIMAL_NUMBER, FALSE);
		UART__Write('\n'); UART__Write('\r');

		InUseMem=0;
		CurrMem=CurrMem^1;
	}
}

void ADC3_CH12_DMA_Config(void)
{
	/* ADC3 configuration */
	ADC_InitTypeDef       ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	DMA_InitTypeDef       DMA_InitStructure;
	GPIO_InitTypeDef      GPIO_InitStructure;
	NVIC_InitTypeDef      NVIC_InitStructure;

	/* Enable ADC3, DMA2 and GPIO clocks */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2 | RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC3, ENABLE);

	/* DMA2 Stream0 channel0 configuration */
	DMA_InitStructure.DMA_Channel = DMA_Channel_2;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC3_DR_ADDRESS;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)(&ADCMem);
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = BUFFER_SIZE;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_DoubleBufferModeConfig(DMA2_Stream0, (uint32_t)(&ADCMem[BUFFER_SIZE]), DMA_Memory_0);
	DMA_DoubleBufferModeCmd(DMA2_Stream0, ENABLE);
	DMA_Init(DMA2_Stream0, &DMA_InitStructure);

	DMA_Cmd(DMA2_Stream0, ENABLE);

	//Interrupt on transfer complete enable
	DMA_ITConfig(DMA2_Stream0, DMA_IT_TC, ENABLE);

	/* Configure ADC3 Channel12 pin as analog input */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* ADC Common Init */
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	/* ADC3 Init */
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = DISABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_Rising;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T2_TRGO;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 1;
	ADC_Init(ADC3, &ADC_InitStructure);

	/* ADC3 regular channel12 configuration */
	ADC_RegularChannelConfig(ADC3, ADC_Channel_12, 1, ADC_SampleTime_15Cycles);

	/* Enable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(ADC3, ENABLE);

	/* Configure Interrupt. Enable and set DMA Interrupt to the lowest priority */
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void TIM_Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	/* Time base configuration */
	TIM_TimeBaseStructure.TIM_Period = 1903;	//Muestreo de la se�al a 44.117KHz.
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);
	TIM_SelectOutputTrigger(TIM2, TIM_TRGOSource_Update);

	/* TIM2 enable counter */
	TIM_Cmd(TIM2, ENABLE);
}

void DMA2_Stream0_IRQHandler(void)
{
	if(DMA_GetITStatus(DMA2_Stream0, DMA_IT_TCIF0)==SET)
	{
		ADCNewBlock=1;
		DMA_ClearITPendingBit(DMA2_Stream0, DMA_IT_TCIF0);
	}
}
