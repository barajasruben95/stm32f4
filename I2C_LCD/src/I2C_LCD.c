#include <stm32f4xx.h>
#include <stm32f4xx_i2c.h>
#include "Delay.h"
#include "I2C.h"
#include "I2C_LCD.h"

uint8_t estadoActual; 	//Almacena el ultimo dato transmitido por la I2C
uint8_t formatoBitsLCD; // Si formatoBitsLCD = 0 -> LCD en modo 8 bits. Si formatoBitsLCD = 1 -> LCD en modo 4 bits

/*
 * Inicializar pantalla LCD encendida en formato de 4 bits y con el cursor encendido
 * 		Requisitos previos->	I2C__Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
*/
void I2C__LCD_Init(uint8_t address)
{
	Delay__Init(); //Inicializar funciones para delays

	formatoBitsLCD = 0; //Al encenderse el modulo LCD siempre comienza en modo de 8 bits

//	//Comenzar con el modulo PCF8574 apagado y con el LCD encendido
//	I2C__Start(address, I2C_Direction_Transmitter);
//	I2C__Write(0b00001000);
//	estadoActual = 0b00001000;
//	I2C__Stop();

	//Inicializar pantalla LCD. Se debe enviar el comando 0b00110000 tres veces con retardos diferentes
	Delay__MS(20);
	I2C__LCD_Escribir(address, 0b00110000, 0);
	Delay__MS(5);
	I2C__LCD_Escribir(address, 0b00110000, 0);
	Delay__US(200);
	I2C__LCD_Escribir(address, 0b00110000,0);

	//Configuraciones principales de la pantalla
	I2C__LCD_Escribir(address, 0b00101000, 0); 	//LCD en modo 4 bits, 2 lineas en pantalla y caracteres en formato 5x8 puntos
	formatoBitsLCD = 1;							//Actualizar variable en modo 4 bits
	I2C__LCD_Escribir(address, 0b00000110, 0); 	//Incrementar el cursor a la derecha cada vez que se escriba un caracter
	I2C__LCD_CursorON(address);
	I2C__LCD_Clear(address);
}

/*
 * Escribir un dato puro (sin conocer si es ASCII o numero). Se traducira en el valor de la tabla de
 * codigos CGROM contenida en la LCD
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 								data:		dato a escribir
 * 		Salida->				Ninguno
*/
void I2C__LCD_Write(uint8_t address, uint8_t data)
{
	I2C__LCD_Escribir(address, data, 1);
}

/*
 * Escribir una cadena de datos que se visualizaran en la pantalla
* 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada->				*data:	direccion del primer dato que sera escrito
* 		Salida->				Ninguno
*/
void I2C__LCD_WriteString(uint8_t address, uint8_t *data)
{
	//Esperar hasta que todos los datos esten escritos
	while (*data)
		I2C__LCD_Escribir(address, *data++, 1);
}

/*
 * Escribir un numero en formato decimal
 *		Requisitos previos->	I2C__LCD_Init();
 *								Activar la unidad de punto flotante
 * 		Entrada->				address:		direccion I2C del dispositivo a comunicarse
 * 								number:			numero a ser escrito. El maximo numero permitido es de 32bits
 * 								decimalDigits:	cantidad de digitos decimales del numero que desean visualizarse en la pantalla
 * 		Salida->				Ninguno
*/
void I2C__LCD_WriteFloatingNumber(uint8_t address, float number, uint8_t decimalDigits)
{
	//Saber si el numero es negativo
	if (number < 0.0)
	{
		I2C__LCD_Escribir(address, '-', 1); //Escribir en la LCD el signo menos
		number *= -1;						//Cambiar el numero a positivo para facil procesamiento
	}

	//Redondeo hacia arriba (Si le llega el numero 1.999 con redondeo de 2, imprime 2.00)
	float rounding = 0.5;
	for(uint8_t i = 0; i < decimalDigits; ++i)
		rounding /= 10.0;
	number += rounding;

	//Extraer la parte entera e imprimir
	uint32_t integerPart = (uint32_t)number;
	uint8_t datos[10];
	int8_t i = 0;
	if(integerPart == 0) 	//Si la parte entera es cero, simplemente visualizarlo
		I2C__LCD_Escribir(address, '0', 1);
	else					//Si la parte entera es diferente de cero, se deben encuentan los digitos enteros
	{
		//Extraer los digitos enteros y almacenarlos en la array datos
		while(integerPart != 0)
		{
			datos[i] = (integerPart % 10);
			integerPart = (integerPart - datos[i]) / 10;
			i++;
		}
		//Imprimir los digitos enteros en la pantalla LCD
		for(i--; i > -1; i--)
			I2C__LCD_Escribir(address, datos[i] + '0', 1);
	}

	//Imprimir punto decimal si es necesario
	if (decimalDigits > 0)
		I2C__LCD_Escribir(address, '.', 1);

	//Extraer los digitos decimales e imprimirlos
	integerPart = (uint32_t)number;
	float remainder = number - (float)integerPart;
	while (decimalDigits-- > 0)
	{
		remainder *= 10.0;
		int32_t toPrint = (int32_t)(remainder);
		I2C__LCD_Escribir(address, toPrint + '0', 1);
		remainder -= toPrint;
	}
}

/*
 * Borra toda la pantalla, memoria DDRAM y pone el cursor al comienzo de la Line 1
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
*/
void I2C__LCD_Clear(uint8_t address)
{
	I2C__LCD_Escribir(address, 0b00000001, 0);
}

/*
 * Cursor al comienzo de la Line 1
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
*/
void I2C__LCD_Line1(uint8_t address)
{
	I2C__LCD_Escribir(address, 0b10000000, 0); //Posicionar cursor en la direccion 00h de la DDRAM
}

/*
 * Cursor al comienzo de la Line 2
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
*/
void I2C__LCD_Line2 (uint8_t address)
{
	I2C__LCD_Escribir(address, 0b11000000, 0); //Posicionar cursor en la direccion 40h de la DDRAM
}

/*
 * Desplazar el cursor cierta cantidad de posiciones en la linea 1 desde el comienzo de la linea
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 								position:	numero de posiciones a desplazar el cursor
 * 		Salida->				Ninguno
*/
void I2C__LCD_positionLine1(uint8_t address, uint8_t position)
{
	I2C__LCD_Escribir(address, 0b10000000 + position, 0); //Direccion 00h de la DDRAM mas el valor de la posicion deseada
}

/*
 * Desplazar el cursor cierta cantidad de posiciones en la linea 2 desde el comienzo de la linea
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 								position:	numero de posiciones a desplazar el cursor
 * 		Salida->				Ninguno
*/
void I2C__LCD_positionLine2(uint8_t address, uint8_t position)
{
	I2C__LCD_Escribir(address, 0b11000000 + position, 0); //Direccion 40h de la DDRAM mas el valor de la posicion deseada
}

/*
 * Apagar la pantalla LCD
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
*/
void I2C__LCD_OFF(uint8_t address)
{
	I2C__LCD_Escribir(address, 0b00001000, 0);
}

/*
 * Pantalla encendida y cursor encendido
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
*/
void I2C__LCD_CursorON(uint8_t address)
{
	I2C__LCD_Escribir(address, 0b00001110, 0);
}

/*
 * Pantalla encendida y cursor apagado
 * 		Requisitos previos->	I2C__LCD_Init();
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 		Salida->				Ninguno
*/
void I2C__LCD_CursorOFF(uint8_t address)
{
	I2C__LCD_Escribir(address, 0b00001100, 0);
}

//**************************
//*** FUNCIONES PRIVADAS ***
//**************************

/*
 * Escribir comando o dato en la LCD
 * 		Entrada-> 				address:	direccion I2C del dispositivo a comunicarse
 * 								dato:		dato a escribir
 * 								tipo:		si tipo = 0 -> se desea escribir un comando.  Si tipo = 1 -> se desea escribir un dato
 * 		Salida->				Ninguno
*/
/*
 * CONEXION DE LA PANTALLA LCD AL MODULO PCF8574
 * __________________________
 * | PIN LCD | PIN PCF8574 	|
 * | D4		 | P4			|
 * | D5		 | P5			|
 * | D6		 | P6			|
 * | D7		 | P7			|
 * | RS		 | P0			|
 * | RW		 | P1			|
 * | E		 | P2			|
 * | VEE	 | P3			|
*/
void I2C__LCD_Escribir(uint8_t address, uint8_t dato, uint8_t tipo)
{
	estadoActual = 0b00001000; //Solamente mantener encendido el contraste de la LCD

	I2C__Start(address, I2C_Direction_Transmitter);

	//Verificar si lo que se va a escribir es dato o comando
	if(tipo == 0)
	{
		estadoActual = LCD_DesactivarRS | estadoActual; //RS = 0. Se va a escribir un comando
		I2C__Write(estadoActual);
	}
	else
	{
		estadoActual = LCD_ActivarRS | estadoActual; //RS = 1. Se va a escribir un dato
		I2C__Write(estadoActual);
	}

	estadoActual = LCD_ActivarE | estadoActual; //Generar flanco de subida en el pin E.
	I2C__Write(estadoActual);

	estadoActual = estadoActual | (dato & 0b11110000); //Mandar dato parte alta
	I2C__Write(estadoActual);

	estadoActual = LCD_DesactivarE & estadoActual; //Capturar dato (flanco de bajada en el pin E) sin perdida de informacion
	I2C__Write(estadoActual);

	I2C__Stop();

	//Si es dato o comando puede colocarse un delay distinto para ahorrar tiempo
	if(tipo == 0)
		Delay__MS(2); 	//Delay para un comando
	else
		Delay__US(50);	//Delay para un dato

	//Si la LCD esta en formato de 4 bits, se debe enviar la parte baja
	if(formatoBitsLCD == 1)
	{
		estadoActual = estadoActual & 0b00001111; //Solamente mantener el estado de los pines VEE, E, RW y RS

		I2C__Start(address, I2C_Direction_Transmitter);

		estadoActual = LCD_ActivarE | (estadoActual & 0b00001001); //Generar flanco de subida en el pin E
		I2C__Write(estadoActual);

		estadoActual = estadoActual | (dato << 4); //Mandar dato parte baja
		I2C__Write(estadoActual);

		estadoActual = LCD_DesactivarE & estadoActual; //Capturar dato (flanco de bajada en el pin E) sin perdida de informacion
		I2C__Write(estadoActual);

		I2C__Stop();

		//Si es dato o comando puede colocarse un delay distinto para ahorrar tiempo
		if(tipo == 0)
			Delay__MS(2);	//Delay para un comando
		else
			Delay__US(50);	//Delay para un dato
	}
}
