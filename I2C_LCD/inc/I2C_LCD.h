#ifndef I2C_LCD_H_
#define I2C_LCD_H_

#define LCD_ActivarRS			0b00000001
#define LCD_DesactivarRS		0b00000000
#define LCD_ActivarE			0b00000100
#define LCD_DesactivarE			0b11111001

//Funciones para el usuario
void I2C__LCD_Init(uint8_t address);

void I2C__LCD_Clear(uint8_t address);
void I2C__LCD_Line1(uint8_t address);
void I2C__LCD_Line2 (uint8_t address);
void I2C__LCD_positionLine1(uint8_t address, uint8_t position);
void I2C__LCD_positionLine2(uint8_t address, uint8_t position);
void I2C__LCD_OFF(uint8_t address);
void I2C__LCD_CursorON(uint8_t address);
void I2C__LCD_CursorOFF(uint8_t address);

void I2C__LCD_Write(uint8_t address, uint8_t data);
void I2C__LCD_WriteString(uint8_t address, uint8_t *data);
void I2C__LCD_WriteFloatingNumber(uint8_t address, float number, uint8_t decimalDigits);

//Funciones privadas
void I2C__LCD_Escribir(uint8_t address, uint8_t dato, uint8_t tipo);

#endif
