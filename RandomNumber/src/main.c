#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

int main(void)
{
	asm("nop");

	uint32_t random32bit = 0;

	//Relojes.
	RCC_AHB2PeriphClockCmd(RCC_AHB2Periph_RNG, ENABLE);

	//Habilitar modulos.
	RNG_Cmd(ENABLE);

	while (1)
	{
		//Esperar a que el modulo RNG este listo.
		while(RNG_GetFlagStatus(RNG_FLAG_DRDY) == RESET);

		//Obtener el numero random de 32 bits.
		random32bit = RNG_GetRandomNumber();

		asm("nop");
	}
}
