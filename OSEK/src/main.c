#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "OS.h"

#define TareaA		0
#define TareaB		1
#define MailboxAB	0

void comenzarTareaA(void);
void comenzarTareaB(void);

int main(void)
{
	//Inicializar LEDs
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED4);
	STM_EVAL_LEDInit(LED5);
	STM_EVAL_LEDOff(LED3);
	STM_EVAL_LEDOff(LED4);
	STM_EVAL_LEDOff(LED5);

	//Crear tareas y asignar atributos.
	Task__Create(TareaA, 5, AUTOSTART_TRUE, &comenzarTareaA);
	Task__Create(TareaB, 1, AUTOSTART_TRUE, &comenzarTareaB);

	//Crear mailbox entre tareaA (Productor) y tareaB(Consumidor).
	MailBox__Create(MailboxAB, TareaA, TareaB);

	//Arrancar sistema operativo.
	OS__Init();

	while(1);
}

void comenzarTareaA(void)
{
	//PROBAR MAILBOXES.
	asm("nop");
	MailBox__Write(MailboxAB, 100);		//Intentar escribir en la memoria compartida. ACCESO HABILITADO primera corrida.
	asm("nop");
	MailBox__Write(MailboxAB, 200);		//Intentar escribir en la memoria compartida. ACCESO DENEGADO primera corrida. ACCESO HABILITADO segunda corrida.
	asm("nop");
	Task__TerminateTask();
}

void comenzarTareaB(void)
{
	//PROBAR MAILBOXES.
	static volatile uint32_t datoMailBox;
	asm("nop");
	datoMailBox = MailBox__Read(MailboxAB);	//Intentar leer de la memoria compartida. ACCESO DENEGADO primera corrida. ACCESO HABILITADO segunda corrida.
	asm("nop");
	datoMailBox = MailBox__Read(MailboxAB);	//Intentar leer de la memoria compartida. ACCESO HABILITADO primera corrida.
	asm("nop");
	Task__TerminateTask();
}
