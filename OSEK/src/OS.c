#include "stm32f4xx.h"
#include "stm32f4_discovery.h"
#include "OS.h"

TASK arregloTareas[BUFFER_TASK];		//Arreglo de tareas del tama�o de BUFFER_TASK.
ALARM arregloAlarmas[BUFFER_ALARM];		//Arreglo de alarmas del tama�o de BUFFER_ALARM.
MAILBOX arregloMailBox[BUFFER_MAILBOX];	//Arreglo de mailboxes del tama�o de BUFFER_MAILBOX.

volatile uint32_t estadoInicialSP;		//Mantener el Stack Pointer sin cambios mientras se corre el OS.
volatile uint32_t estadoMovibleSP;

TASK* apuntadorTareaActual;				//Apuntador que contiene la tarea actual en ejecucion.
uint8_t contadorTareas = 0;				//Cantidad de tareas creadas y almacenadas en arregloTareas.
uint8_t contadorAlarmas = 0;			//Cantidad de alarmas creadas y almacenadas en arregloAlarmas.
uint8_t contadorMailBoxes = 0;			//Cantidad de mailboxes creados y almacenadas en arregloMailBox.

NVIC_InitTypeDef NVIC_InitStructure;	//Vector de interrupciones.

//INICIALIZACION DEL SISTEMA OPERATIVO.
void OS__Init(void)
{
	estadoInicialSP = estadoMovibleSP = __GetStackPointer();	//Stack pointer del sistema operativo. Al final de todas las tareas, el stack pointer debe ser igual a estadoInicialSP.

	//Tarea actual con algun valor.
	apuntadorTareaActual = &arregloTareas[0];

	OS__SetAutoStartTask();
	OS__Scheduler();
}

//CREAR TAREA Y METERLA DENTRO DEL ARREGLO DE TAREAS.
void Task__Create(uint8_t taskIndex, uint8_t priority, uint8_t autoStart, void(*taskAddress)(void))
{
	TASK newTask;
	newTask.priority = priority;
	newTask.autoStart = autoStart;
	newTask.state = STATE_IDDLE;
	newTask.status = STATUS_NOT_EXECUTED;
	newTask.startAddress = taskAddress;

	arregloTareas[taskIndex] = newTask;
	contadorTareas++; 								//Tarea creada, incrementar la cantidad de tareas totales.
}

//CREAR ALARMA Y METERLA DENTRO DEL ARREGLO DE ALARMAS.
void Alarm__Create(uint8_t alarmIndex, uint32_t reload, uint8_t repeat, uint8_t status, uint8_t taskIndex)
{
	ALARM newAlarm;
	newAlarm.counter = reload;
	newAlarm.reload = reload;
	newAlarm.repeat = repeat;
	newAlarm.status = status;
	newAlarm.taskAddress = &arregloTareas[taskIndex];

	arregloAlarmas[alarmIndex] = newAlarm;
	contadorAlarmas++; 									//Alarma creada, incrementar la cantidad de alarmas totales.
}

//CREAR MAILBOX.
void MailBox__Create(uint8_t mailBoxIndex, uint8_t producerTaskIndex, uint8_t consumerTaskIndex)
{
	MAILBOX newMailBox;
	newMailBox.semaphoreStatus = SEMAPHORE_FREE;		//Semaforo habilitado para el productor.
	newMailBox.sharedMemory = 0;
	newMailBox.taskProducer = &arregloTareas[producerTaskIndex];
	newMailBox.taskConsumer = &arregloTareas[consumerTaskIndex];

	arregloMailBox[mailBoxIndex] = newMailBox;
	contadorMailBoxes++;
}

//PRODUCTOR INTENTA ESCRIBIR EN LA MEMORIA COMPARTIDA.
void MailBox__Write(uint8_t mailBoxIndex, uint8_t data)
{
	//Si el semaforo indica libertad en la memoria compartida, el productor puede escribir en la memoria compartida.
	//Si el semaforo impide acceso a la memoria compartida, la tarea en curso debe ser mandada a WAIT y correr el Scheduler.
	if(arregloMailBox[mailBoxIndex].semaphoreStatus == SEMAPHORE_FREE)
	{
		arregloMailBox[mailBoxIndex].sharedMemory = data;
		arregloMailBox[mailBoxIndex].semaphoreStatus = SEMAPHORE_BUSY;
		arregloMailBox[mailBoxIndex].taskConsumer->state = STATE_READY;		//Decirle al consumidor que tiene un dato en el semaforo.
	}
	else
	{
		//Guardar contexto.
		apuntadorTareaActual->returnAddress = __GetLinkRegister() - 10;

		apuntadorTareaActual->state = STATE_WAIT;
		apuntadorTareaActual->status = STATUS_NOT_FINISHED;
		OS__Scheduler();
	}
}

void funcion(void)
{
	asm("nop");
}

//EL CONSUMIDOR INTENTA LEER DE LA MEMORIA COMPARTIDA.
uint32_t MailBox__Read(uint8_t mailBoxIndex)
{
	static volatile uint32_t datoCompartido;
	register uint32_t guardar;
	static volatile uint32_t guardar2;
	//Si el semaforo indica libertad en la memoria compartida, indica que el productor no ha escrito. Mandar tarea en curso a WAIT y correr scheduler.
	//Si el semaforo impide acceso a la memoria compartida, el consumidor podra leer el dato.
	if(arregloMailBox[mailBoxIndex].semaphoreStatus == SEMAPHORE_BUSY)
	{
		arregloMailBox[mailBoxIndex].semaphoreStatus = SEMAPHORE_FREE;		//Liberar semaforo.

		//Si la tarea productor no ha finalizado su ejecucion, se le debe indicar que el semaforo esta libre.
		if(arregloMailBox[mailBoxIndex].taskProducer->status != STATUS_EXECUTED)
			arregloMailBox[mailBoxIndex].taskProducer->state = STATE_READY;
		apuntadorTareaActual->state = STATE_READY;				//Tarea actual pasarla a READY porque tiene que volver a lanzar el scheduler.
		apuntadorTareaActual->status = STATUS_NOT_FINISHED;		//Tarea actual no termino de ejecutarse.
		datoCompartido = arregloMailBox[mailBoxIndex].sharedMemory;

		__asm volatile ("MOV %0, SP\n" : "=r" (guardar) );
		apuntadorTareaActual->SP = guardar;
		estadoMovibleSP = guardar;
		__asm volatile ("MOV %0, r7\n" : "=r" (guardar) );
		guardar2 = guardar;

		funcion(); //Obtener direccion de la siguiente instruccion (LR).
		apuntadorTareaActual->returnAddress = __GetLinkRegister() + 17;
		OS__Scheduler();

		guardar = apuntadorTareaActual->SP;
		__asm volatile ("MOV SP, %0\n" : : "r" (guardar) );
		estadoMovibleSP = estadoInicialSP;
		guardar = guardar2;
		__asm volatile ("MOV r7, %0\n" : : "r" (guardar) );
	}
	else
	{
		//Guardar contexto.
		apuntadorTareaActual->returnAddress = __GetLinkRegister() - 9;

		apuntadorTareaActual->state = STATE_WAIT;
		apuntadorTareaActual->status = STATUS_NOT_FINISHED;
		OS__Scheduler();
	}
	return datoCompartido;
}

//CAMBIAR LAS TAREAS QUE TENGAN AUTOSTART_TRUE DE STATE_IDDLE A STATE_READY.
void OS__SetAutoStartTask(void)
{
	uint8_t j;
	for(j = 0; j < contadorTareas; j++)
	{
		if(arregloTareas[j].autoStart == AUTOSTART_TRUE)
			arregloTareas[j].state = STATE_READY;
	}
}

//BUSCAR TAREAS CON READY EN EL SCHEDULER PARA COMENZAR A EJECUTARLAS.
void OS__Scheduler(void)
{
	//No guardar nada en el stack.
	__asm volatile ("MOV R12,%0 " ::"r"(estadoMovibleSP):"r12");
	asm ("MOV SP, R12");

	static uint8_t i;
	static uint8_t auxiliar;
	static uint8_t flagTareaLista;						//Bandera para saber si se selecciono alguna tarea correctamente.

	//BUSCAR TAREAS POR STATE_READY Y PRIORIDAD, LA VARIABLE AUXILIAR CONTENDRA EL INDEX + 1 DE LA TAREA DENTRO DEL ARREGLO QUE DEBERA EJECUTARSE.
	buscarTarea:
	flagTareaLista = 0;									//Saber si el scheduler encontro alguna tarea en ready. 1: Si encontro una tarea en ready. 0: No encontro ninguna tarea en ready.
	auxiliar = 200;
	for(i = 0; i < contadorTareas; i++)
	{
		if(arregloTareas[i].state == STATE_READY)
		{
			if(arregloTareas[i].priority < auxiliar)
			{
				auxiliar = arregloTareas[i].priority;
				flagTareaLista = i + 1;
			}
		}
	}

	//GARANTIZAR QUE SE SELECCIONO ALGUNA TAREA.
	if(flagTareaLista != 0)
	{
		apuntadorTareaActual = &arregloTareas[flagTareaLista - 1];	//Guardar la tarea que se va a ejecutar en un direccionContexto.
		apuntadorTareaActual->state = STATE_RUN;					//Momentaneamente poner la tarea en RUN. Posteriormente se revisara si esta tiene o no memoria compartida.
	}
	else
		goto buscarTarea; 											//ALGO MAL. No hay tareas con STATE_READY en el scheduler. Volver a buscar.

	//REVISAR SI LA TAREA NO HA SIDO EJECUTADA O SI FUE PAUSADA POR EL SCHEDULER.
	if(apuntadorTareaActual->status == STATUS_NOT_EXECUTED)
	{
		apuntadorTareaActual->startAddress();							//Saltar a la direccionRetorno asignada para correr la tarea seleccionada.
	}
	else if(apuntadorTareaActual->status == STATUS_NOT_FINISHED)
	{
		//RESTAURAR CONTEXTO PRACTICA TAREAS, MAILBOXES.
		__SetProgramCounter(apuntadorTareaActual->returnAddress);
	}
	else
	{
		apuntadorTareaActual->status = STATUS_EXECUTED; 				//ALGO MAL. Se selecciono una tarea que ya fue ejecutada. Cambiarla de estado a ejecutada.
		goto buscarTarea;												//Buscar otra tarea para ejecutar.
	}
}

//CAMBIAR TAREA ACTUAL DE STATE_RUN A STATE_READY. CAMBIAR TAREA NUEVA DE STATE_IDDLE A STATE_READY.
void Task__ActivateTask(uint8_t taskIndex)
{
	apuntadorTareaActual->returnAddress = __GetLinkRegister(); 		//Guardar la ultima instruccion ejecutada de la tarea que fue mandada llamar (apuntadorTareaActual->returnAddress = LR).
	apuntadorTareaActual->state = STATE_READY;						//Cambiar la tarea actual de STATE_RUN a STATE_READY.
	apuntadorTareaActual->status = STATUS_NOT_FINISHED;				//Cambiar la tarea actual a STATUS_NOT_FINISHED.
	arregloTareas[taskIndex].state = STATE_READY;					//Cambiar nueva tarea de STATE_IDDLE a STATE_READY.
	OS__Scheduler();
}

//CAMBIAR TAREA NUEVA DE STATE_IDDLE A STATE_READY.
void Task__ActivateTask_Interrupt(uint8_t taskIndex)
{
	apuntadorTareaActual->state = STATE_READY;						//Cambiar la tarea actual de STATE_RUN a STATE_READY.
	apuntadorTareaActual->status = STATUS_NOT_FINISHED;				//Cambiar la tarea actual a STATUS_NOT_FINISHED.
	arregloTareas[taskIndex].state = STATE_READY;					//Cambiar nueva tarea de STATE_IDDLE a STATE_READY.
	OS__Scheduler();
}

//TERMINAR LA TAREA ACTUAL. CAMBIAR LA NUEVA TAREA DE STATE_IDDLE A STATE_READY.
void Task__ChainTask(uint8_t taskIndex)
{
	apuntadorTareaActual->state = STATE_IDDLE;				//Cambiar la tarea actual de STATE_RUN a STATE_IDDLE.
	apuntadorTareaActual->status = STATUS_EXECUTED;			//Cambiar la tarea actual a STATUS_EXECUTED.
	arregloTareas[taskIndex].state = STATE_READY;			//Cambiar nueva tarea de STATE_IDDLE a STATE_READY.
	OS__Scheduler();
}

//TERMINAR LA TAREA ACTUAL.
void Task__TerminateTask(void)
{
	apuntadorTareaActual->state = STATE_IDDLE;				//Cambiar la tarea actual de STATE_RUN a STATE_IDDLE.
	apuntadorTareaActual->status = STATUS_EXECUTED;			//Cambiar la tarea actual a STATUS_EXECUTED.
	OS__Scheduler();
}

//OBTENER EL DATO QUE GUARDA EL REGISTRO LR DEL CORE.
__attribute__( ( always_inline ) ) static inline uint32_t __GetLinkRegister(void)
{
	register uint32_t result;
	__asm volatile ("MOV %0, LR\n" : "=r" (result) );
	return(result);
}

//OBTENER EL DATO QUE GUARDA EL REGISTRO SP DEL CORE.
__attribute__( ( always_inline ) ) static inline uint32_t __GetStackPointer(void)
{
	register uint32_t result;
	__asm volatile ("MOV %0, SP\n" : "=r" (result) );
	return(result);
}

//ESCRIBIR UNA DIRECCION EN EL PC DEL CORE PARA SALTAR A LA ULTIMA INSTRUCCION EJECUTADA DE ALGUNA TAREA QUE NO TERMINO SU EJECUCION.
__attribute__( ( always_inline ) ) static inline void __SetProgramCounter(uint32_t value)
{
	register uint32_t address = value;
	__asm volatile ("MOV PC, %0\n" : : "r" (address) );
}

//ESCRIBIR UNA DIRECCION EN EL SP.
__attribute__( ( always_inline ) ) static inline void __SetStackPointer(uint32_t value)
{
	register uint32_t spValue = value;
	__asm volatile ("MOV SP, %0\n" : : "r" (spValue) );
}
