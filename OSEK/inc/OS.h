#ifndef OS_H_
#define OS_H_

#define BUFFER_TASK 	5
#define BUFFER_ALARM 	5
#define BUFFER_MAILBOX 	5

#define FALSE				0
#define TRUE				1

#define AUTOSTART_FALSE 	0
#define AUTOSTART_TRUE 		1

#define STATE_IDDLE 		0
#define STATE_READY 		1
#define STATE_RUN 			2
#define STATE_WAIT 			3

#define STATUS_NOT_EXECUTED	0		//La tarea no ha sido ejecutada.
#define STATUS_NOT_FINISHED	1		//La tarea fue ejecutada pero no termino.
#define STATUS_EXECUTED		2		//La tarea ya fue ejecutada.

#define ALARM_NOT_REPEAT	0		//Deshabilitar repeticiones de la alarma.
#define ALARM_REPEAT		1		//Habilitar repeticion de la alarma.

#define ALARM_DISABLE		0		//Deshabilitar alarma.
#define ALARM_ENABLE		1		//Habilitar alarma.

#define SEMAPHORE_FREE		0		//Semaforo libre para poder escribir.
#define SEMAPHORE_BUSY		1		//Semaforo ocupado para poder escribir.

#define OS_Scheduler(); alarmaEncontrada=1

typedef struct
{
	uint32_t returnAddress;			//Direccion de retorno cuando la tarea no fue completamente ejecutada.
	uint32_t SP;					//Direccion donde se encuentra el primer dato guardado en el SP. Para recuperar contexto.
	void(*startAddress)(void);		//Direccion donde se encuentran las instrucciones de la tarea.
	uint8_t priority;				//Prioridad de la tarea
	uint8_t autoStart;				//Autocomenzar tarea.
	uint8_t state;					//Estado de la tarea: STATE_IDDLE, STATE_READY, STATE_RUN o STATE_WAIT.
	uint8_t status;					//Para saber si la tarea ya ha sido ejecutada al menos una vez: STATUS_NOT_EXECUTED, STATUS_NOT_FINISHED o STATUS_EXECUTED.
} TASK;

typedef struct
{
	uint32_t counter;				//Decrementos del contador.
	uint32_t reload;				//Valor de recarga cada vez que se terminen los decrementos en ms.
	TASK* taskAddress;				//Direccion donde se encuentra la tarea que debe activar.
	uint8_t repeat;					//Repeticion de la alarma: ALARM_NOT_REPEAT y ALARM_REPEAT.
	uint8_t status;					//Estado de la alarma: ALARM_DISABLE y ALARM_ENABLE.
} ALARM;

typedef struct
{
	uint32_t sharedMemory;			//Dato compartido.
	uint8_t semaphoreStatus;		//Estado del semaforo: SEMAPHORE_FREE y SEMAPHORE_BUSY.
	TASK* taskConsumer;
	TASK* taskProducer;
}MAILBOX;

void OS__Init(void);
void OS__SetAutoStartTask(void);
void OS__Scheduler(void);
void Task__Create(uint8_t taskIndex, uint8_t priority, uint8_t autoStart, void(*taskAddress)(void));
void MailBox__Create(uint8_t mailBoxIndex, uint8_t producerTaskIndex, uint8_t consumerTaskIndex);
void MailBox__Write(uint8_t mailBoxIndex, uint8_t data);
uint32_t MailBox__Read(uint8_t mailBoxIndex);
void Task__ActivateTask(uint8_t taskIndex);
void Task__ActivateTask_Interrupt(uint8_t taskIndex);
void Task__ChainTask(uint8_t taskIndex);
void Task__TerminateTask(void);
__attribute__( ( always_inline ) ) static inline uint32_t __GetLinkRegister(void);
__attribute__( ( always_inline ) ) static inline uint32_t __GetStackPointer(void);
__attribute__( ( always_inline ) ) static inline void __SetProgramCounter(uint32_t value);
__attribute__( ( always_inline ) ) static inline void __SetStackPointer(uint32_t value);

#endif
