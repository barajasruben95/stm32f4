# FPU activation

1. Open **project properties**. Add two new preprocesor symbols over the following path: **C/C++ Build->Settings->MCU GCC Compiler->Preprocessor**. The preprocessor symbols are **__FPU_USED = 1** and **__FPU_PRESENT = 1**
https://bitbucket.org/barajasruben95/stm32f4/downloads/fpu_01.jpg
https://bitbucket.org/barajasruben95/stm32f4/downloads/fpu_02.jpg
https://bitbucket.org/barajasruben95/stm32f4/downloads/fpu_03.jpg
https://bitbucket.org/barajasruben95/stm32f4/downloads/fpu_04.jpg

1. Look inside the file **system_stm32f4xxx.c** and at the end of the function **SystemInit** include the following code:
```C
#if (__FPU_USED == 1)
  	SCB->CPACR |= (3UL << 20) | (3UL << 22);
   	 __DSB();
    	__ISB();
#endif
```
https://bitbucket.org/barajasruben95/stm32f4/downloads/fpu_05.jpg
