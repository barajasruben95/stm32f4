#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
TIM_OCInitTypeDef TIM_OCStruct;

void BasicTimers__Config(void);
void Leds_Init(void);
void TM_PWM_Init(void);

int main(void)
{
	uint32_t tiempo;

	Leds_Init();
	BasicTimers__Config();
	TM_PWM_Init();

	while(1)
	{
		if (TIM_GetFlagStatus(TIM1, TIM_IT_Update) != RESET)
		{
			TIM_ClearITPendingBit(TIM1, TIM_IT_Update);

			STM_EVAL_LEDToggle(LED4);
			tiempo++;
		}

		if(tiempo > 10)
		{
			tiempo = 0;
			TIM1->CCR2 = 0xFF; //No se puede modificar el registro. Nivel de seguridad 3.
		}
	}
}

void BasicTimers__Config(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	TIM_BDTRInitTypeDef TIM_BDTRInitStructure;

	TIM_TimeBaseInitStruct.TIM_Prescaler = 8399;
	TIM_TimeBaseInitStruct.TIM_Period = 9999;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStruct);

	TIM_BDTRStructInit(&TIM_BDTRInitStructure);
	TIM_BDTRConfig(TIM1, &TIM_BDTRInitStructure);
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
	TIM_Cmd(TIM1, ENABLE);
}

void Leds_Init(void)
{
	STM_EVAL_LEDInit(LED3);
	STM_EVAL_LEDInit(LED4);
	STM_EVAL_LEDInit(LED5);
	STM_EVAL_LEDInit(LED6);

	STM_EVAL_LEDOff(LED3);
	STM_EVAL_LEDOff(LED4);
	STM_EVAL_LEDOff(LED5);
	STM_EVAL_LEDOff(LED6);
}

void TM_PWM_Init(void)
{
	TIM_OCStruct.TIM_OCMode = TIM_OCMode_PWM2;
	TIM_OCStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCStruct.TIM_OCPolarity = TIM_OCPolarity_Low;

	TIM_OCStruct.TIM_Pulse = 2099;
	TIM_OC1Init(TIM1, &TIM_OCStruct);
	TIM_OC1PreloadConfig(TIM1, TIM_OCPreload_Enable);
}
