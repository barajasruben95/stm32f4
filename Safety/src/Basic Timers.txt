#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

void BasicTimers__Config(void);

int main(void)
{
	while(TIM_GetCounter(TIM6) == TIM_GetCounter(TIM7))
	{
		asm("nop");
	}

	while(1);
}

void BasicTimers__Config(void)
{
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM6, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM7, ENABLE);

	//TIM6 y TIM7 tienen una frecuencia de reloj de 84MHz.
	//La resolucion de TIM6 y TIM7 es de 16 bits con preescalador de 16 bits.
	//Tanto TIM6 como TIM7 no tienen canales.
	//La frecuencia de interrupcion de TIM6 y TIM6 estara dada por: TIMx_Clock / ((TIM_Prescaler + 1) * (TIM_Period + 1)).
	//Si se quiere una interrupcion cada 1s (1Hz) los datos quedan como: 84MHz / ((8399 + 1) * (9999 + 1)) = 1Hz.

	TIM_TimeBaseInitStruct.TIM_Prescaler = 8399;
	TIM_TimeBaseInitStruct.TIM_Period = 9999;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM6, &TIM_TimeBaseInitStruct);

	TIM_TimeBaseInitStruct.TIM_Prescaler = 8399;
	TIM_TimeBaseInitStruct.TIM_Period = 9999;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM7, &TIM_TimeBaseInitStruct);

	TIM_Cmd(TIM6, ENABLE);	//Habilitar TIM6.
	TIM_Cmd(TIM7, ENABLE);	//Habilitar TIM7.
}
