#include "stm32f4xx.h"
#include "UART.h"

int main(void)
{
	uint8_t prueba[] = {"PRUEBA\n\r"};
	uint32_t numero = 4294967;

	UART__Init();

	UART__WriteString(&prueba[0]);
	UART__WriteFloatingNumber(-12345.678, 4);
	UART__Write('\n'); UART__Write('\r');

	//Formato de numeros
	UART__WriteNumber(numero, HEXADECIMAL_NUMBER, TRUE);
	UART__Write('\n'); UART__Write('\r');
	UART__WriteNumber(numero, DECIMAL_NUMBER, TRUE);
	UART__Write('\n'); UART__Write('\r');
	UART__WriteNumber(numero, OCTAL_NUMBER, FALSE);
	UART__Write('\n'); UART__Write('\r');
	UART__WriteNumber(numero, BINARY_NUMBER, TRUE);
	UART__Write('\n'); UART__Write('\r');

	while(1)
	{
		UART__Write(UART__Read());
	}
}
