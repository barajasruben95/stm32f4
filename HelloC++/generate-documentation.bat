@echo off
title Generate Doxygen documentation

echo Check if doc folder is created
IF EXIST \doc (
echo Folder doc already exists 
) ELSE (
echo Creating folder doc
mkdir doc
)

echo Check if doc\dox folder is created
IF EXIST \doc\custom (
echo Folder custom already exists 
) ELSE (
echo Creating folder custom
mkdir doc\custom
)

echo Check if doc\html folder is created
IF EXIST \doc\html (
echo Folder custom already exists 
) ELSE (
echo Creating folder images
mkdir doc\html
)

echo Check if doc\images folder is created
IF EXIST \doc\images (
echo Folder custom already exists 
) ELSE (
echo Creating folder images
mkdir doc\images
)

echo Deletes previous html generation
del /q/f/s doc\html
echo Unused files removed

echo Deletes previous warnings generation
del /q/f/s doc\warnings.log
echo File removed

echo Deletes previous cache generation
del /q/f/s doc\inline_umlgraph_cache_all.pu
echo File removed

echo Run Doxygen
doxygen Doxyfile

echo Open web browser with results
start "" doc\html\index.html
