/**
 ******************************************************************************
 * @file    main.c
 * @author  Ruben Barajas
 * @version V1.0
 * @date    18-September-2020
 * @brief   Default main function.
 ******************************************************************************
 */

#include "stm32f4xx.h"
#include "Testing.hpp"

int main(void)
{
	Family familia1 = Family();
	Family familia2 = Family(20);

	uint8_t edadFamilia1 = familia1.getEdad();
	uint8_t edadFamilia2 = familia2.getEdad();

	for(;;);
}
