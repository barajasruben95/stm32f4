/**
 ******************************************************************************
 * @file    Testing.cpp
 * @author  Ruben Barajas
 * @version V1.0
 * @date    18-September-2020
 * @brief   Testing only
 ******************************************************************************
 */

#include "Testing.hpp"

Family::Family()
{
	edad = 10;
}

Family::Family(uint8_t edad)
{
	this->edad = edad;
}

Family::~Family()
{
	//Destructor was called
}

void Family::setEdad(uint8_t edad)
{
	this->edad = edad;
}

uint8_t Family::getEdad()
{
	return edad;
}
