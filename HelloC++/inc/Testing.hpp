/**
 ******************************************************************************
 * @file    Testing.hpp
 * @author  Ruben Barajas
 * @version V1.0
 * @date    18-September-2020
 ******************************************************************************
 */

#ifndef TESTING_HPP_
#define TESTING_HPP_

#include "stm32f4xx.h"

/**
 * Class Family the basis to create a family
 */
class Family
{
private:
	uint8_t edad; /// Stores the age of the member

public:
	/**
	 * Default constructor. Sets age at 10.
	 */
	Family();

	/**
	 * Overloaded constructor.
	 * \param edad Age at the beginning of the object
	 */
	Family(uint8_t edad);

	/**
	 * Destructor kills the object to freeup the heap and avoid memory leaks
	 */
	~Family();

	/**
	 * Setter for the param edad
	 * \param edad Age that wants to be set
	 */
	void setEdad(uint8_t edad);

	/**
	 * Getter for the param edad
	 * \return Current age of the member
	 */
	uint8_t getEdad();

};

#endif /* TESTING_HPP_ */
