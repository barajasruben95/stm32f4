#ifndef ONEWIRE_H

#define ONEWIRE_SEARCH_ROM			0xF0 //Conocer las memorias ROM de multiples dispositivos conectados al bus
#define ONEWIRE_READ_ROM			0x33 //Conocer la memoria ROM de un unico dispositivo conectado al bus
#define ONEWIRE_MATCH_ROM			0x55 //Seleccionar el esclavo mediante la identificacion de los 64 bits de identificacion
#define ONEWIRE_SKIP_ROM			0xCC //Saltarse la seleccion de un esclavo para poder comunicarse con muchos al mismo tiempo o cuando solo hay uno conectado al bus
#define ONEWIRE_ALARM_SEARCH		0xEC //Determinar si algun esclavo conectado al bus sufrio una condicion de alarma

#define ONEWIRE_NO_ERROR_RESET		0X00
#define ONEWIRE_ERROR_RESET			0X01
#define ONEWIRE_DEVICE_NOT_FOUND	0X00
#define ONEWIRE_DEVICE_FOUND		0X01
#define ONEWIRE_CRC_NO_ERROR		0X00
#define ONEWIRE_CRC_ERROR			0X01

#define TRUE	1
#define FALSE	0

void OneWire__Init(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);

uint8_t OneWire__Reset(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);
void OneWire__Write(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t byte);
uint8_t OneWire__Read(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);

void OneWire__WriteBit(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t bit);
uint8_t OneWire__ReadBit(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin);

//Comandos OneWire
uint8_t OneWire__SearchROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);
uint8_t OneWire__ReadROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t *addressROM);
uint8_t OneWire__MatchROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t *firstAddressROM);
uint8_t OneWire__SkipROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);
uint8_t OneWire__SearchAlarm(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);

uint8_t OneWire__CRC8(uint8_t *firstData);
uint8_t OneWire__GetROM(uint8_t index);
void OneWire__GetFullROM(uint8_t *firstAddressROM);
uint8_t OneWire__First(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);
uint8_t OneWire__Next(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);

//Funciones privadas
uint8_t OneWire__Search(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t command);
void OneWire__Alto(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);
void OneWire__Bajo(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);

#endif
