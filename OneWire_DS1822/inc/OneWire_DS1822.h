#ifndef ONEWIRE_DS1822_H_
#define ONEWIRE_DS1822_H_

typedef struct
{
	float temperature;
	uint8_t resolution;
	uint8_t romAdddress[8];
	uint8_t powerMode;
} DS1822;

#define DS1822_CONVERT_TEMPERATURE		0x44 //Comenzar conversion de temperatura
#define DS1822_WRITE_SCRATCHPAD			0x4E //Escribir en el registro TH, TL y configuracion
#define DS1822_READ_SCRATCHPAD			0xBE //Leer los registros de temperatura, TH, TL, configuracion y CRC
#define DS1822_COPY_SCRATCHPAD			0x48 //Copiar los registros TH, TL y configuracion en la memoria EEPROM
#define DS1822_RECALL_EEPROM			0xB8 //Restaurar los registros TH, TL y configuracion desde la memoria EEPROM
#define DS1822_READ_POWER_SUPLY			0xB4 //Leer si algun dispositivo esta utilizando alimentacion parasita

#define DS1822_ERROR				0x01
#define DS1822_PARASITE_POWER		0x10
#define DS1822_NORMAL_POWER			0x20

#define DS1822_RESOLUTION_9BITS			0x1F
#define DS1822_RESOLUTION_10BITS		0x3F
#define DS1822_RESOLUTION_11BITS		0x5F
#define DS1822_RESOLUTION_12BITS		0x7F

#define DS1822_DECIMAL_STEPS_9BITS		0.5
#define DS1822_DECIMAL_STEPS_10BITS		0.25
#define DS1822_DECIMAL_STEPS_11BITS		0.125
#define DS1822_DECIMAL_STEPS_12BITS		0.0625

//Funciones de usuario
uint8_t OneWire__DS1822_Init(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822);
uint8_t OneWire__DS1822_ReadTemperature(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822);
uint8_t OneWire__DS1822_ReadPowerSuply(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822);
uint8_t OneWire__DS1822_SetConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, float limitTemperature, uint8_t sensorResolution, DS1822 *newDS1822);
uint8_t OneWire__DS1822_GetConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, float *limitTemperature, uint8_t *sensorResolution, DS1822 *newDS1822);
uint8_t OneWire__DS1822_SaveConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822);
uint8_t OneWire__DS1822_RecallConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822);


//Funciones privadas
void OneWire__DS1822_ConvertTemperature(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822);
void OneWire__DS1822_CopyScratchpad(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822);
void OneWire__DS1822_WriteScratchpad(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);
uint8_t OneWire__DS1822_ReadScratchpad(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);
void OneWire__DS1822_RecallEEPROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x);

#endif
