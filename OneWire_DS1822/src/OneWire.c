#include <stm32f4xx.h>
#include <stm32f4xx_gpio.h>
#include "Delay.h"
#include "OneWire.h"

uint8_t lastDiscrepancy = 0;
uint8_t lastDeviceFlag = 0;
uint8_t lastFamilyDiscrepancy = 0;
uint8_t oneWire_romAddress[8];

//Pines para master pull up
GPIO_TypeDef *masterPullUpGPIOx;
uint16_t masterPullUpGPIO_Pin_x;

GPIO_InitTypeDef GPIO_InitStructure;

//****************************************************************************************************
void OneWire__Init(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	Delay__Init();

	// Reloj del pin
	if(GPIOx == GPIOA)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	else if(GPIOx == GPIOB)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
	else if(GPIOx == GPIOC)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	else if(GPIOx == GPIOD)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
	else if(GPIOx == GPIOE)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
	else if(GPIOx == GPIOF)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF, ENABLE);
	else if(GPIOx == GPIOG)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG, ENABLE);
	else if(GPIOx == GPIOH)
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH, ENABLE);
	else
	{
		if(GPIOx == GPIOI)
			RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOI, ENABLE);
	}

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_x;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOx, &GPIO_InitStructure);
}

//****************************************************************************************************
uint8_t OneWire__Reset(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	uint8_t i;

	//Linea en cero y esperar 480us
	OneWire__Bajo(GPIOx, GPIO_Pin_x);
	Delay__US(480);

	//Linea en uno y esperar 70us
	OneWire__Alto(GPIOx, GPIO_Pin_x);
	Delay__US(70);

	//Verificar el bit de presencia
	i = GPIO_ReadInputDataBit(GPIOx, GPIO_Pin_x);

	//Retardo de 410us
	Delay__US(410);

	//Retornar el pulso de presencia. Si es 0 = OK, si es 1 = ERROR
	return i;
}

//****************************************************************************************************
void OneWire__Write(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t byte)
{
	uint8_t i = 8;

	while (i--)
	{
		//Primero escribir el bit LSB
		OneWire__WriteBit(GPIOx, GPIO_Pin_x, byte & 0x01);
		byte >>= 1;
	}
}

//****************************************************************************************************
uint8_t OneWire__Read(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	uint8_t i = 8;
	uint8_t byte = 0;

	while (i--)
	{
		byte >>= 1;
		byte |= (OneWire__ReadBit(GPIOx, GPIO_Pin_x) << 7);
	}

	return byte;
}

//****************************************************************************************************
void OneWire__WriteBit(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t bit)
{
	//Poner linea en bajo
	OneWire__Bajo(GPIOx, GPIO_Pin_x);

	if (bit)
	{
		//Linea en bajo por 10us
		Delay__US(10);

		//Poner el bit en alto y esperar 55us
		OneWire__Alto(GPIOx, GPIO_Pin_x);
		Delay__US(55);
	}
	else
	{
		//Linea en bajo por 65us
		Delay__US(65);

		//Poner el bit en alto y esperar 5us
		OneWire__Alto(GPIOx, GPIO_Pin_x);
		Delay__US(5);
	}

	//Dejar la linea
	OneWire__Alto(GPIOx, GPIO_Pin_x);
}

//****************************************************************************************************
uint8_t OneWire__ReadBit(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	uint8_t bit = 0;

	OneWire__Bajo(GPIOx, GPIO_Pin_x);
	Delay__US(3);

	//Dejar la linea por 10us
	OneWire__Alto(GPIOx, GPIO_Pin_x);
	Delay__US(10);

	//Leer el valor del bit
	if (GPIO_ReadInputDataBit(GPIOx, GPIO_Pin_x))
		bit = 1;

	//Esperar 50us para completar 60us del periodo de lectura del bit
	Delay__US(50);

	return bit;
}

//****************************************************************************************************
uint8_t OneWire__SearchROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	return 1;
}

//****************************************************************************************************
uint8_t  OneWire__ReadROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t *firstAddressROM)
{
	//Primero el pulso de reset
	if(OneWire__Reset(GPIOx, GPIO_Pin_x))
		return ONEWIRE_ERROR_RESET; //Error en el pulso de reset
	else
	{
		//Pedirle al esclavo su direccion ROM
		OneWire__Write(GPIOx, GPIO_Pin_x, ONEWIRE_READ_ROM);

		//Lectura y almacenamiento de la ROM
		uint8_t i;
		for(i = 0; i < 8; i++)
			*(firstAddressROM + i) = OneWire__Read(GPIOx, GPIO_Pin_x);

		return ONEWIRE_NO_ERROR_RESET; //Lectura de la ROM correctamente
	}
}

//****************************************************************************************************
uint8_t OneWire__MatchROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t *firstAddressROM)
{
	//Primero el pulso de reset
	if(OneWire__Reset(GPIOx, GPIO_Pin_x))
		return ONEWIRE_ERROR_RESET; //Error en el pulso de reset
	else
	{
		//Indicarle al esclavo que sera seleccionado por su ROM
		OneWire__Write(GPIOx, GPIO_Pin_x, ONEWIRE_MATCH_ROM);

		//Seleccionar el esclavo
		uint8_t i;
		for (i = 0; i < 8; i++)
			OneWire__Write(GPIOx, GPIO_Pin_x, *(firstAddressROM + i));

		return ONEWIRE_NO_ERROR_RESET; //Seleccion del esclavo correcta
	}
}

//****************************************************************************************************
uint8_t OneWire__SkipROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	//Primero el pulso de reset
	if(OneWire__Reset(GPIOx, GPIO_Pin_x))
		return ONEWIRE_ERROR_RESET; //Error en el pulso de reset
	else
	{
		//Indicarle al bus que no se seleccionara ningun esclavo en particular
		OneWire__Write(GPIOx, GPIO_Pin_x, ONEWIRE_SKIP_ROM);

		return ONEWIRE_NO_ERROR_RESET; //Ningun esclavo seleccionado correctamente ejecutado
	}
}

//****************************************************************************************************
uint8_t OneWire__SearchAlarm(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	return 1;
}

//****************************************************************************************************
uint8_t OneWire__CRC8(uint8_t *firstData)
{
	uint8_t inbyte, mix;
	uint8_t crc = 0;
	uint8_t length = 8;
	uint8_t i;

	while (length--)
	{
		inbyte = *firstData++;
		for (i = 8; i; i--)
		{
			mix = (crc ^ inbyte) & 0x01;
			crc >>= 1;
			if (mix)
				crc ^= 0x8C;
			inbyte >>= 1;
		}
	}

	return crc;
}

/**
 * @brief  Starts search, reset states first
 * @note   When you want to search for ALL devices on one onewire port, you should first use this function.
\code
//...Initialization before
status = TM_OneWire_First(&OneWireStruct);
while (status) {
	//Save ROM number from device
	TM_OneWire_GetFullROM(ROM_Array_Pointer);
	//Check for new device
	status = TM_OneWire_Next(&OneWireStruct);
}
\endcode
 * @param  *OneWireStruct: Pointer to @ref TM_OneWire_t working onewire where to reset search values
 * @param  Device status:
 *            - 0: No devices detected
 *            - > 0: Device detected
 */
//****************************************************************************************************
uint8_t OneWire__First(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	lastDiscrepancy = 0;
	lastDeviceFlag = 0;
	lastFamilyDiscrepancy = 0;

	//Comenzar busqueda
	return OneWire__Search(GPIOx, GPIO_Pin_x, ONEWIRE_SEARCH_ROM);
}

//****************************************************************************************************
uint8_t OneWire__Next(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
   //Comenzar busqueda de un posible dispositivo mas
   return OneWire__Search(GPIOx, GPIO_Pin_x, ONEWIRE_SEARCH_ROM);
}

//****************************************************************************************************
uint8_t OneWire__Search(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, uint8_t command)
{
	uint8_t id_BitNumber;
	uint8_t lastZero, romByteNumber, searchResult;
	uint8_t id_Bit, cmp_id_Bit;
	uint8_t romByteMask, searchDirection;

	//Initialize for search
	id_BitNumber = 1;
	lastZero = 0;
	romByteNumber = 0;
	romByteMask = 1;
	searchResult = 0;

	//Check if any devices
	if (!lastDeviceFlag)
	{
		//One Wire reset
		if (OneWire__Reset(GPIOx, GPIO_Pin_x))
		{
			//Reset the search
			lastDiscrepancy = 0;
			lastDeviceFlag = 0;
			lastFamilyDiscrepancy = 0;
			return ONEWIRE_ERROR_RESET;
		}

		//Issue the search command
		OneWire__Write(GPIOx, GPIO_Pin_x, command);

		//Loop to do the search
		do
		{
			//Read a bit and its complement
			id_Bit = OneWire__ReadBit(GPIOx, GPIO_Pin_x);
			cmp_id_Bit = OneWire__ReadBit(GPIOx, GPIO_Pin_x);

			//Check for no devices on One Wire
			if ((id_Bit == 1) && (cmp_id_Bit == 1))
			{
				break;
			}
			else
			{
				//All devices coupled have 0 or 1
				if (id_Bit != cmp_id_Bit)
				{
					//Bit write value for search
					searchDirection = id_Bit;
				}
				else
				{
					//If this discrepancy is before the Last Discrepancy on a previous next then pick the same as last time
					if (id_BitNumber < lastDiscrepancy)
					{
						searchDirection = ((oneWire_romAddress[romByteNumber] & romByteMask) > 0);
					}
					else
					{
						//If equal to last pick 1, if not then pick 0
						searchDirection = (id_BitNumber == lastDiscrepancy);
					}

					//If 0 was picked then record its position in LastZero
					if (searchDirection == 0)
					{
						lastZero = id_BitNumber;

						//Check for Last discrepancy in family
						if (lastZero < 9)
						{
							lastFamilyDiscrepancy = lastZero;
						}
					}
				}

				//Set or clear the bit in the ROM byte romByteNumber with mask romByteMask
				if (searchDirection == 1)
				{
					oneWire_romAddress[romByteNumber] |= romByteMask;
				}
				else
				{
					oneWire_romAddress[romByteNumber] &= ~romByteMask;
				}

				//Serial number search direction write bit
				OneWire__WriteBit(GPIOx, GPIO_Pin_x, searchDirection);

				//Increment the byte counter id_BitNumber and shift the mask romByteMask
				id_BitNumber++;
				romByteMask <<= 1;

				//If the mask is 0 then go to new SerialNum byte romByteNumber and reset mask
				if (romByteMask == 0)
				{
					romByteNumber++;
					romByteMask = 1;
				}
			}
		//Loop until through all ROM bytes 0-7
		} while (romByteNumber < 8);

		//* If the search was successful then
		if (!(id_BitNumber < 65))
		{
			//Search successful so set lastDiscrepancy, lastDeviceFlag, searchResult
			lastDiscrepancy = lastZero;

			//Check for last device
			if (lastDiscrepancy == 0)
			{
				lastDeviceFlag = 1;
			}

			searchResult = 1;
		}
	}

	//If no device found then reset counters so next 'search' will be like a first
	if (!searchResult || !oneWire_romAddress[0])
	{
		lastDiscrepancy = 0;
		lastDeviceFlag = 0;
		lastFamilyDiscrepancy = 0;
		searchResult = 0;
	}

	return searchResult;
}

//****************************************************************************************************
void OneWire__Alto(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	GPIOx->MODER &= 0xFFFFFFFC;
}

void OneWire__Bajo(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	GPIOx->ODR &= 0x1111111E;
	GPIOx->MODER = (GPIOx->MODER & 0xFFFFFFFD) | 0x00000001;
}
