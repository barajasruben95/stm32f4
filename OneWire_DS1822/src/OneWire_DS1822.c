#include <stm32f4xx.h>
#include <stm32f4xx_gpio.h>
#include "Delay.h"
#include "OneWire.h"
#include "OneWire_DS1822.h"
#include "UART.h"

uint8_t data[9];

//****************************************************************************************************
uint8_t OneWire__DS1822_Init(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822)
{
	Delay__Init();

	//Leer la memoria ROM del DS1822
	if(OneWire__ReadROM(GPIOA, GPIO_Pin_0, newDS1822->romAdddress) == ONEWIRE_ERROR_RESET)
		return DS1822_ERROR; //Error de lectura de la ROM

	//Leer el tipo de alimentacion del DS1822
	if(OneWire__DS1822_ReadPowerSuply(GPIOA, GPIO_Pin_0, newDS1822) == DS1822_ERROR)
		return DS1822_ERROR;

	return 0;
}

//****************************************************************************************************
uint8_t OneWire__DS1822_ReadTemperature(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822)
{
	uint8_t sign = 0;
	int8_t digit;
	uint16_t temperatura;

	//Pulso de reset y seleccionar el dispositivo
	if(OneWire__MatchROM(GPIOx, GPIO_Pin_x, newDS1822->romAdddress) == ONEWIRE_ERROR_RESET)
		return DS1822_ERROR;

	//Convertir temperatura
	OneWire__DS1822_ConvertTemperature(GPIOx, GPIO_Pin_x, newDS1822);

	//Pulso de reset y seleccionar el dispositivo
	if(OneWire__MatchROM(GPIOx, GPIO_Pin_x, newDS1822->romAdddress) == ONEWIRE_ERROR_RESET)
		return DS1822_ERROR;

	//Leer datos del scratchpad
	OneWire__DS1822_ReadScratchpad(GPIOx, GPIO_Pin_x);

	//Temperatura en formato de 16 bits
	temperatura = data[0] | (data[1] << 8);

	//Verificar si la temperatura es negativa
	if (temperatura & 0x8000)
	{
		//Complemento a dos porque la temperatura es negativa
		temperatura = ~temperatura + 1;
		sign = 1;
	}

	//Obtener la resolucion del sensor
	newDS1822->resolution = ((data[4] & 0x60) >> 5) + 9;

	//Almacenar digitos parte entera
	digit = temperatura >> 4;
	digit |= ((temperatura >> 8) & 0x07) << 4;

	//Almacenar digitos parte decimal
	if(newDS1822->resolution == 9)
	{
		newDS1822->temperature = (temperatura >> 3) & 0x01;
		newDS1822->temperature *= (float)DS1822_DECIMAL_STEPS_9BITS;
	}
	else if(newDS1822->resolution == 10)
	{
		newDS1822->temperature = (temperatura >> 2) & 0x03;
		newDS1822->temperature *= (float)DS1822_DECIMAL_STEPS_10BITS;
	}
	else if(newDS1822->resolution == 11)
	{
		newDS1822->temperature = (temperatura >> 1) & 0x07;
		newDS1822->temperature *= (float)DS1822_DECIMAL_STEPS_11BITS;
	}
	else if(newDS1822->resolution == 12)
	{
		newDS1822->temperature = temperatura & 0x0F;
		newDS1822->temperature *= (float)DS1822_DECIMAL_STEPS_12BITS;
	}
	else
	{
		newDS1822->temperature = 0xFF;
		digit = 0;
	}

	//Acomodo final de los datos
	newDS1822->temperature = digit + newDS1822->temperature;
	if (sign)
		newDS1822->temperature = -1 * newDS1822->temperature;

	//Lectura de temperatura correcta
	return 0;
}

//****************************************************************************************************
uint8_t OneWire__DS1822_ReadPowerSuply(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822)
{
	//Pulso de reset y seleccionar el dispositivo
	if(OneWire__MatchROM(GPIOx, GPIO_Pin_x, newDS1822->romAdddress) == ONEWIRE_ERROR_RESET)
		return DS1822_ERROR;

	//Comando leer fuente de alimentacion
	OneWire__Write(GPIOx, GPIO_Pin_x, DS1822_READ_POWER_SUPLY);

	//Guardar el tipo de fuente de alimentacion del dispositivo: parasita o normal
	if(!OneWire__ReadBit(GPIOx, GPIO_Pin_x))
		newDS1822->powerMode = DS1822_PARASITE_POWER;
	else
		newDS1822->powerMode = DS1822_NORMAL_POWER;

	return newDS1822->powerMode;
}

//****************************************************************************************************
uint8_t OneWire__DS1822_SetConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, float limitTemperature, uint8_t sensorResolution, DS1822 *newDS1822)
{
	return DS1822_ERROR;
}

//****************************************************************************************************
uint8_t OneWire__DS1822_GetConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, float *limitTemperature, uint8_t *sensorResolution, DS1822 *newDS1822)
{
	return DS1822_ERROR;
}

//****************************************************************************************************
uint8_t OneWire__DS1822_SaveConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822)
{
	return DS1822_ERROR;
}

//****************************************************************************************************
uint8_t OneWire__DS1822_RecallConfiguration(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822)
{
	return DS1822_ERROR;
}

//****************************************************************************************************
//****************************************************************************************************
//****************************************************************************************************
void OneWire__DS1822_ConvertTemperature(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822)
{
	//Comando convertir temperatura
	OneWire__Write(GPIOx, GPIO_Pin_x, DS1822_CONVERT_TEMPERATURE);

	//Revisar modo parasito
	if(newDS1822->powerMode == DS1822_PARASITE_POWER)
	{
		//Master pull up y tiempo de conversion
		OneWire__Alto(GPIOx, GPIO_Pin_x);
		Delay__MS(1000);
	}
	else
		//Esperar conversion de temperatura
		while (!OneWire__ReadBit(GPIOx, GPIO_Pin_x));
}

//****************************************************************************************************
void OneWire__DS1822_CopyScratchpad(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x, DS1822 *newDS1822)
{
	//Comando copiar scratchpad
	OneWire__Write(GPIOx, GPIO_Pin_x, DS1822_COPY_SCRATCHPAD);

	//Revisar modo parasito
	if(newDS1822->powerMode == DS1822_PARASITE_POWER)
	{
		//Master pull up y tiempo de conversion
		OneWire__Alto(GPIOx, GPIO_Pin_x);
		Delay__MS(1000);
	}
	else
		//Esperar conversion de temperatura
		while (!OneWire__ReadBit(GPIOx, GPIO_Pin_x));
}

//****************************************************************************************************
void OneWire__DS1822_WriteScratchpad(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	//Comando escribir scratchpad
	OneWire__Write(GPIOx, GPIO_Pin_x, DS1822_WRITE_SCRATCHPAD);

	//Escritura de los bytes desde TH, TL y Configuration
	uint8_t i;
	for(i = 0; i < 3; i++)
		OneWire__Write(GPIOx, GPIO_Pin_x, data[i]);
}

//****************************************************************************************************
uint8_t OneWire__DS1822_ReadScratchpad(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	//Comando leer scratchpad
	OneWire__Write(GPIOx, GPIO_Pin_x, DS1822_READ_SCRATCHPAD);

	//Lectura y almacenamiento de los 9 bytes
	uint8_t i;
	for(i = 0; i < 9; i++)
		data[i] = OneWire__Read(GPIOx, GPIO_Pin_x);

	//Revisar el CRC
	if(OneWire__CRC8(&data[0]) != data[8])
		return ONEWIRE_CRC_ERROR;
	else
		return ONEWIRE_CRC_NO_ERROR;
}

//****************************************************************************************************
void OneWire__DS1822_RecallEEPROM(GPIO_TypeDef *GPIOx, uint16_t GPIO_Pin_x)
{
	//Comando recall EEPROM
	OneWire__Write(GPIOx, GPIO_Pin_x, DS1822_RECALL_EEPROM);

	//Esperar que los datos sean copiados de la memoria EEPROM
	while (!OneWire__ReadBit(GPIOx, GPIO_Pin_x));
}

