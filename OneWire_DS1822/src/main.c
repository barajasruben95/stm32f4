#include "stm32f4xx.h"
#include <Delay.h>
#include <UART.h>
#include <OneWire.h>
#include <OneWire_DS1822.h>

int main(void)
{
	uint8_t ok[]={"OK\n\r"};
	uint8_t error[]={"ERROR\n\r"};
	uint8_t parasito[]={"PARASITE\n\r"};
	DS1822 myDS1822;
	uint8_t i;

	UART__Init();
	OneWire__Init(GPIOA, GPIO_Pin_0);

	//Inicializacion del sensor de temperatura
	if(OneWire__DS1822_Init(GPIOA, GPIO_Pin_0, &myDS1822) == DS1822_ERROR)
	{
		UART__WriteString(&error[0]);//Inicializacion incorrecta
		while(1);
	}
	else
	{
		UART__WriteString(&ok[0]); //Inicializacion correcta

		//Mostrar la direccion ROM del sensor
		for(i = 0; i < 8; i++)
		{
			UART__WriteNumber(myDS1822.romAdddress[i], HEXADECIMAL_NUMBER, FALSE); UART__Write('\n'); UART__Write('\r');
		}

		//Mostrar el modo de alimentacion en caso de ser parasito
		if(myDS1822.powerMode == DS1822_PARASITE_POWER)
		{
			UART__WriteString(&parasito[0]);
		}
	}

	while(1)
	{
		//Leer la temperatura del sensor
		if(OneWire__DS1822_ReadTemperature(GPIOA, GPIO_Pin_0, &myDS1822) == DS1822_ERROR)
		{
			UART__WriteString(&error[0]); UART__Write('\n'); UART__Write('\r'); //Lectura incorrecta
		}
		else
		{
			//Visualizar la temperatura en consola
			UART__WriteFloatingNumber(myDS1822.temperature, 4); UART__Write('\n'); UART__Write('\r');
		}

		//Tiempo entre conversiones de temperatura
		Delay__MS(1000);
	}
}
