/* Tabla con la que se comprobaron los resultados:
https://docs.google.com/spreadsheets/d/1Q9OOF0VQ6WiqCzlwTeqPgev3-IAV4xLCKWfQK6jePls/edit?usp=sharing */

#include "stm32f4xx.h"

#define cantidadDatos 38

const uint32_t entradaSeno[cantidadDatos] =
{
		//Positivos desde 0� hasta 90� con incrementos de 5�.
		0x00000000,
		0x0595C613,
		0x0B2B8C25,
		0x10C15238,
		0x1657184A,
		0x1BECDE5E,
		0x2182A471,
		0x27186A83,
		0x2CAE3096,
		0x3243F6A8,
		0x37D9BCBB,
		0x3D6F82CE,
		0x430548E0,
		0x489B0EF4,
		0x4E30D506,
		0x53C69B19,
		0x595C612C,
		0x5EF2273E,
		0x6487ED51,

		//Negativos desde 0� hasta 90� con incrementos de 5�.
		0x80000000,
		0x8595C613,
		0x8B2B8C25,
		0x90C15238,
		0x9657184A,
		0x9BECDE5E,
		0xA182A471,
		0xA7186A83,
		0xACAE3096,
		0xB243F6A8,
		0xB7D9BCBB,
		0xBD6F82CE,
		0xC30548E0,
		0xC89B0EF4,
		0xCE30D506,
		0xD3C69B19,
		0xD95C612C,
		0xDEF2273E,
		0xE487ED51
};

uint32_t seno(uint32_t x)
{
	register uint32_t data;
	data = x;

	__asm volatile ("MOV R1, %0\n" : : "r" (data)); 	//Almacenar valor de x en R1.
	__asm	(
			"mov r11, LR\n"
			"and r10, r1, #0x80000000\n"	//Guardar signo.
			"and r1, r1, 0x7FFFFFFF\n"		//Quitar signo.

			"principal:"
			"mov r2, r1\n"
			"mov r3, r1\n"
			"bl multiplicacion\n"
			"mov r0, r2\n"				//Almacenar valor de x^2 en R0.
			////////////////////////////
			"ldr r3, =0x01861861\n"		//Cargar valor 1/42.
			"bl multiplicacion\n"
			"bl resta\n"
			////////////////////////////
			"mov r2, r0\n"
			"ldr r3, =0x03333333\n"		//Cargar valor de 1/20.
			"bl multiplicacion\n"
			"mov r3, r12\n"
			"bl multiplicacion\n"
			"bl resta\n"
			////////////////////////////
			"mov r2, r0\n"
			"ldr r3, =0x0AAAAAA4\n"		//Cargar valor de 1/6.
			"bl multiplicacion\n"
			"mov r3, r12\n"
			"bl multiplicacion\n"
			"bl resta\n"
			////////////////////////////
			"mov r2, r1\n"
			"mov r3, r12\n"
			"bl multiplicacion\n"
			"bl final\n"

			//Multiplicacion recibe en r2 y r3 los operandos a multiplicar. Retorna resultado en r2.
			"multiplicacion:"
			"push {LR}\n"
			"umull r2, r3, r2, r3\n"
			"lsrs r2, r2, #2\n"
			"lsrs r3, r3, #2\n"
			"lsrs r2, r2, #28\n"
			"lsls r3, r3, #4\n"
			"orr r2, r2, r3\n"
			"pop {PC}\n"

			//Resta recibe en r2 el valor del operando para aplicar la operacion 1-r2. Retorna resultado en r12.
			"resta:"
			"push {LR}\n"
			"rsb r2, r2, #1\n"
			"and r2, #0x3FFFFFFF\n"
			"sub r12, r2, #1\n"
			"pop {PC}\n"

			"final:"
			"orr r2, r2, r10\n"
			"mov LR, r11\n"
	);
	__asm volatile ("MOV %0, R2\n" : "=r" (data) );

	return data;
}

int main(void)
{
	uint32_t salidaSeno[cantidadDatos];
	uint8_t i;

	for(i = 0; i < cantidadDatos; i++)
	{
		salidaSeno[i] = seno(entradaSeno[i]);
	}

	while(1);
}
