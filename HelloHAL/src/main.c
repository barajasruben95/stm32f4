/**
 ******************************************************************************
 * @file    main.c
 * @author  Ac6
 * @version V1.0
 * @date    01-December-2013
 * @brief   Default main function.
 ******************************************************************************
 */


#include "stm32f4xx.h"
#include "stm32f4_discovery.h"

#include "stm32f4xx_hal.h"

int main(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	/*
	Initialize HAL Library. This step is mandatory
	for HAL libraries to work properly. It configures
	SysTick etc. for its internal configurations.
	 */
	HAL_Init();

	/*
	After HAL_Init(), System is running on default HSI
	clock (16MHz). PLL is disabled.
	 */

	/* Enable clock to GPIO-D */
	__HAL_RCC_GPIOD_CLK_ENABLE();

	/* Set GPIOD Pin#15 Parameters */
	GPIO_InitStruct.Pin     = GPIO_PIN_15;
	GPIO_InitStruct.Mode    = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull    = GPIO_PULLDOWN;
	GPIO_InitStruct.Speed   = GPIO_SPEED_FREQ_LOW;

	/* Init GPIOD Pin#15 */
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

	while (1)
	{
		/* Toggle GPIOD Pin#15 --> BLUE LED on STM32F407-Discovery */
		HAL_GPIO_TogglePin(GPIOD, GPIO_PIN_15);

		/* 1000ms second delay */
		HAL_Delay(1000);
	}
}
